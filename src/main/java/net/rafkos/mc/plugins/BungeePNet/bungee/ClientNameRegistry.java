/**
 * 
 * Copyright (C) 2018 Rafał Kosyl <admin@rafkos.net>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
*/
package net.rafkos.mc.plugins.BungeePNet.bungee;

import java.util.HashMap;

import lombok.Getter;
import nl.pvdberg.pnet.client.Client;

@Getter
public class ClientNameRegistry
{
	private HashMap<Client, String> clientNames = new HashMap<>();
	private HashMap<String, Client> clientNamesReverse = new HashMap<>();

	public synchronized Client getClientByName(String name)
	{
		return this.clientNamesReverse.get(name);
	}

	/**
	 * 
	 * @param c Who
	 * @return Returns client name sent by client (possible Bungee's client name),
	 *         if client not found returns null value
	 */
	public synchronized String getClientName(Client c)
	{
		return this.clientNames.get(c);
	}

	/**
	 * @param c    Client reference
	 * @param name Name of the client which is supposed to correspond with Bungee's
	 *             client names.
	 * @throws InterruptedException
	 */
	public synchronized boolean register(Client c, String name)
	{
		boolean flag = false;
		if(!this.clientNames.containsKey(c))
		{
			this.clientNames.put(c, name);
			this.clientNamesReverse.put(name, c);
			flag = true;
		}
		return flag;
	}

	public synchronized boolean unregister(Client c)
	{
		boolean flag = false;
		if(this.clientNames.containsKey(c))
		{
			this.clientNamesReverse.remove(this.clientNames.get(c));
			this.clientNames.remove(c);
			flag = true;
		}
		return flag;
	}
	
}
