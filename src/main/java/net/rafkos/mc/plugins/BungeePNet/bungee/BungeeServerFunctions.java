/**
 * 
 * Copyright (C) 2018 Rafał Kosyl <admin@rafkos.net>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
*/
package net.rafkos.mc.plugins.BungeePNet.bungee;

import lombok.extern.log4j.Log4j;
import nl.pvdberg.pnet.client.Client;

@Log4j
public class BungeeServerFunctions
{
	public static void registerClient(Client c, String name) throws InterruptedException
	{
		log.info("Registering client " + c + " as '" + name + "'");
		if(!BungeeApi.getClientNameRegistry().register(c, name))
		{
			log.warn("Unable to register client " + c + " as '" + name + "'");
		}
		
	}

	public static void unregisterClient(Client c)
	{
		String name = BungeeApi.getClientNameRegistry().getClientName(c);
		if(BungeeApi.getClientNameRegistry().unregister(c))
		{
			log.info("Client '" + name + "' has been unregistered");
		}
	}
}
