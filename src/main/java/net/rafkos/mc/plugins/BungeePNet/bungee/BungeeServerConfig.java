/**
 * 
 * Copyright (C) 2018 Rafał Kosyl <admin@rafkos.net>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
*/
package net.rafkos.mc.plugins.BungeePNet.bungee;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class BungeeServerConfig
{
	private int hostPort = 6667;
	private boolean TLSEnabled = true;
	private String keyStoreFileName = "default-bungeepnet-keystore.p12";
	private String keyStorePassword = "default";
	private String keyStoreType = "PKCS12";

	public BungeeServerConfig(int hostPort, boolean TLSEnabled, String keyStoreFileName,
			String keyStorePassword, String keyStoreType)
	{
		this.hostPort = hostPort;
		this.TLSEnabled = TLSEnabled;
		this.keyStoreFileName = keyStoreFileName;
		this.keyStorePassword = keyStorePassword;
		this.keyStoreType = keyStoreType;
	}
}
