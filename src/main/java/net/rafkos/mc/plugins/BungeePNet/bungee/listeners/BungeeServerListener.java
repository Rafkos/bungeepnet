/**
 * 
 * Copyright (C) 2018 Rafał Kosyl <admin@rafkos.net>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
*/
package net.rafkos.mc.plugins.BungeePNet.bungee.listeners;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j;
import net.rafkos.mc.plugins.BungeePNet.bungee.BungeeApi;
import net.rafkos.mc.plugins.BungeePNet.bungee.BungeeServerFunctions;
import net.rafkos.mc.plugins.BungeePNet.bungee.handlers.packets.HeartbeatHandler;
import net.rafkos.mc.plugins.BungeePNet.bungee.handlers.packets.RegisterHandler;
import net.rafkos.mc.plugins.BungeePNet.bungee.handlers.packets.RequestHandler;
import net.rafkos.mc.plugins.BungeePNet.bungee.handlers.packets.ResponseHandler;
import net.rafkos.mc.plugins.BungeePNet.common.Config;
import net.rafkos.mc.plugins.BungeePNet.common.RemoteClient;
import net.rafkos.mc.plugins.BungeePNet.common.interfaces.IClientSender;
import nl.pvdberg.pnet.client.Client;
import nl.pvdberg.pnet.event.PNetListener;
import nl.pvdberg.pnet.packet.Packet;

@Getter
@Setter
@Log4j
public class BungeeServerListener implements PNetListener
{
	private static final RegisterHandler registerHandler = new RegisterHandler();
	private static final ResponseHandler responseHandler = new ResponseHandler();
	private static final RequestHandler requestHandler = new RequestHandler();
	private static final HeartbeatHandler heartbeatHandler = new HeartbeatHandler();

	@Override
	public void onConnect(Client c)
	{
		log.info("New client connected: " + c);
		BungeeApi.getHeartbeatMonitorRegistry().registerClient(c);
		BungeeApi.getHeartbeatTransmitterRegistry().registerClient(c);
	}

	@Override
	public void onDisconnect(Client c)
	{
		log.info("Client disconnected: " + c);
		String clientName = BungeeApi.getClientNameRegistry().getClientName(c);
		BungeeApi.getClientConnectionListenerManager().notifyDisconnected(clientName);
		BungeeServerFunctions.unregisterClient(c);
		BungeeApi.getHeartbeatMonitorRegistry().unregisterClient(c);
		BungeeApi.getHeartbeatTransmitterRegistry().unregisterClient(c);
	}

	@Override
	public void onReceive(Packet p, Client c)
	{
		handleReceivedPacket(p, new RemoteClient(c));
	}

	public static void handleReceivedPacket(Packet p, IClientSender c)
	{
		short id = p.getPacketID();
		switch(id)
		{
		case Config.RESPONSE_PACKET_ID:
			responseHandler.handlePacket(p, c);
			break;
		case Config.REGISTER_PACKET_ID:
			registerHandler.handlePacket(p, c);
			String clientName = BungeeApi.getClientNameRegistry().getClientName(((RemoteClient) c).getClient());
			BungeeApi.getClientConnectionListenerManager().notifyConnected(clientName);
			break;
		case Config.REQUEST_PACKET_ID:
			requestHandler.handlePacket(p, c);
			break;
		case Config.HEARTBEAT_PACKET_ID:
			heartbeatHandler.handlePacket(p, c);
			break;
		}
	}

}
