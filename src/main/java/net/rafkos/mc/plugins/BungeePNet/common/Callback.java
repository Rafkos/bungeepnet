/**
 * 
 * Copyright (C) 2018 Rafał Kosyl <admin@rafkos.net>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
*/
package net.rafkos.mc.plugins.BungeePNet.common;

import java.util.HashMap;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j;
import net.rafkos.mc.plugins.BungeePNet.common.interfaces.ICallback;
import net.rafkos.mc.plugins.BungeePNet.common.interfaces.IConsumer;

@AllArgsConstructor
@NoArgsConstructor
@Log4j
public class Callback implements ICallback
{
	private IConsumer<HashMap<String, Object>> resultHandler;
	private IConsumer<Throwable> errorHandler;

	@Override
	public void onComplete(IConsumer<HashMap<String, Object>> resultHandler, IConsumer<Throwable> errorHandler)
	{
		this.resultHandler = resultHandler;
		this.errorHandler = errorHandler;
	}

	public IConsumer<HashMap<String, Object>> getResultHandler()
	{
		if(resultHandler == null)
		{
			return (result) ->
			{
			};
		}
		return resultHandler;
	}

	public IConsumer<Throwable> getErrorHandler()
	{
		if(errorHandler == null)
		{
			return (error) ->
			{
				log.error("A request has respond with an error", error);
			};
		}
		return errorHandler;
	}

}
