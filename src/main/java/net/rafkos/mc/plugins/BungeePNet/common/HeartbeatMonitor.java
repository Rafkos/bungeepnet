/**
 * 
 * Copyright (C) 2018 Rafał Kosyl <admin@rafkos.net>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
*/
package net.rafkos.mc.plugins.BungeePNet.common;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j;
import nl.pvdberg.pnet.client.Client;

@Log4j
public class HeartbeatMonitor
{
	private Client client;
	private long intervalMillis;
	private AtomicLong lastBeatTimeMillis = new AtomicLong(0);
	private ScheduledExecutorService executor;
	private AtomicBoolean active = new AtomicBoolean(true);
	
	public HeartbeatMonitor(Client client, long intervalMillis, ScheduledExecutorService executor)
	{
		this.client = client;
		this.intervalMillis = intervalMillis;
		this.lastBeatTimeMillis.set(System.currentTimeMillis());
		this.executor = executor;
		startMonitor();
	}
	
	private void startMonitor()
	{
		log.info("Starting a heartbeat monitor for "+client.toString());
		executor.schedule(new MonitorTask(), intervalMillis, TimeUnit.MILLISECONDS);
	}
	
	public void terminate()
	{
		active.set(false);
	}

	public void beat()
	{
		lastBeatTimeMillis.set(System.currentTimeMillis());
	}
	
	private class MonitorTask implements Runnable
	{

		@Override
		@SneakyThrows
		public void run()
		{
			mainLoop:
			while(active.get())
			{
				// check for timeout
				if(lastBeatTimeMillis.get() + intervalMillis < System.currentTimeMillis())
				{
					log.warn("Could not receive a beat from client "+client.toString()+" in "+intervalMillis+"ms. Closing client connection.");
					// close client
					client.getSocket().close();
					active.set(false);
				}
				
				for(int i = 0; i < intervalMillis/10; i++)
				{
					if(!active.get())
					{
						log.info("Terminating heartbeat monitor for "+client.toString());
						break mainLoop;
					}
					Thread.sleep(10);
				}
				
			}
		}
		
	}
	
}
