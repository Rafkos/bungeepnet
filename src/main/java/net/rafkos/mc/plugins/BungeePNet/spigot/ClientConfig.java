/**
 * 
 * Copyright (C) 2018 Rafał Kosyl <admin@rafkos.net>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
*/
package net.rafkos.mc.plugins.BungeePNet.spigot;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class ClientConfig
{
	private String clientName = "change_me";
	private String hostIp = "127.0.0.1";
	private int hostPort = 6667;
	private boolean reconnectEnabled = true;
	private long reconnectIntervalMilliseconds = 3000;
	private int reconnectionAttemptsLimit = 5;
	private boolean connectionToServerRequired = true;
	private boolean TLSEnabled = true;
	private String trustStoreFileName = "default-bungeepnet-truststore.p12";
	private String trustStorePassword = "default";
	private String trustStoreType = "PKCS12";

	public ClientConfig(String clientName, String hostIp, int hostPort, boolean reconnectEnabled,
			long reconnectIntervalMilliseconds, int reconnectionAttemptsLimit, boolean connectionToServerRequired,
			boolean TLSEnabled, String trustStoreFileName, String trustStorePassword, String trustStoreType)
	{
		this.clientName = clientName;
		this.hostPort = hostPort;
		this.hostIp = hostIp;
		this.reconnectEnabled = reconnectEnabled;
		this.reconnectIntervalMilliseconds = reconnectIntervalMilliseconds;
		this.reconnectionAttemptsLimit = reconnectionAttemptsLimit;
		this.connectionToServerRequired = connectionToServerRequired;
		this.TLSEnabled = TLSEnabled;
		this.trustStoreFileName = trustStoreFileName;
		this.trustStorePassword = trustStorePassword;
		this.trustStoreType = trustStoreType;
	}

}
