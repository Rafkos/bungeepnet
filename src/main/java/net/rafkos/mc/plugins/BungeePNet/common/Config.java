/**
 * 
 * Copyright (C) 2018 Rafał Kosyl <admin@rafkos.net>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
*/
package net.rafkos.mc.plugins.BungeePNet.common;

public class Config
{
	public static final short REQUEST_PACKET_ID = 10;
	public static final short RESPONSE_PACKET_ID = 20;
	public static final short REGISTER_PACKET_ID = 30;
	public static final short HEARTBEAT_PACKET_ID = 40;
	public static final long HEARTBEAT_INTERVAL_MILLIS = 3000;
	public static final long HEARTBEAT_MONITOR_MILLIS = 20000;
	public static final int DEFAULT_TIMEOUT_MILLIS = 10000;
	public static final int DEFAULT_SIZE_OF_TIMEOUT_EXECUTOR_THREAD_POOL = 4;
	public static final int DEFAULT_SIZE_OF_GENERIC_EXECUTOR_POOL = 2;
	public static final long CALLBACK_WAIT_TASK_TIME_MILLIS = 3000;
	public static final int NUM_OF_CHECKS_BEFORE_TERMINATION_OF_CALLBACKS_REGISTRY = 5;
}
