/**
 * 
 * Copyright (C) 2018 Rafał Kosyl <admin@rafkos.net>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
*/
package net.rafkos.mc.plugins.BungeePNet.bungee;

import java.util.HashMap;
import java.util.Iterator;

import net.rafkos.mc.plugins.BungeePNet.common.Config;
import net.rafkos.mc.plugins.BungeePNet.common.HeartbeatTransmitter;
import nl.pvdberg.pnet.client.Client;

public class ClientHeartbeatTransmitterRegistry
{
	private HashMap<Client, HeartbeatTransmitter> transmitters = new HashMap<>();
	
	public synchronized void registerClient(Client c)
	{
		transmitters.put(c, new HeartbeatTransmitter(c, Config.HEARTBEAT_INTERVAL_MILLIS, BungeeApi.getExecutorService()));
	}
	
	public synchronized void unregisterClient(Client c)
	{
		HeartbeatTransmitter transmitter = null;
		if((transmitter = transmitters.remove(c)) != null)
		{
			transmitter.terminate();
		}
	}
	
	public synchronized void unregisterAll()
	{
		Iterator<Client> it = transmitters.keySet().iterator();
		while(it.hasNext())
		{
			unregisterClient(it.next());
		}
	}
	
	public synchronized HeartbeatTransmitter getHeartbeatMonitor(Client c)
	{
		return transmitters.get(c);
	}
	
}
