/**
 * 
 * Copyright (C) 2018 Rafał Kosyl <admin@rafkos.net>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
*/
package net.rafkos.mc.plugins.BungeePNet.common;

import lombok.SneakyThrows;
import net.rafkos.mc.plugins.BungeePNet.common.interfaces.IAction;

public class ActionBuilder
{
	@SuppressWarnings("unchecked")
	@SneakyThrows
	public static IAction getAction(String actionClassName, Object[] actionParameters)
	{
		if(actionParameters == null)
		{
			actionParameters = new Object[] {};
		}
		
		Class<?>[] parameterTypes = new Class<?>[actionParameters.length];
		for(int i = 0; i < actionParameters.length; i++)
		{
			parameterTypes[i] = actionParameters[i].getClass();
		}
		IAction action = null;

		Class<? extends IAction> actionClass = null;
		actionClass = (Class<? extends IAction>) Class.forName(actionClassName);

		if(actionParameters.length == 0)
		{
			action = actionClass.getConstructor().newInstance();
		}else
		{
			action = actionClass.getConstructor(parameterTypes).newInstance(actionParameters);
		}
		
		return action;
	}
}
