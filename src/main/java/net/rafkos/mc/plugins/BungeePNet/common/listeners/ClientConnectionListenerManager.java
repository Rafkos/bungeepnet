/**
 * 
 * Copyright (C) 2018 Rafał Kosyl <admin@rafkos.net>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
*/
package net.rafkos.mc.plugins.BungeePNet.common.listeners;

import java.util.HashMap;
import java.util.LinkedList;

import net.md_5.bungee.api.plugin.Plugin;
import net.rafkos.mc.plugins.BungeePNet.common.interfaces.IConnectionListenerManager;

public class ClientConnectionListenerManager implements IConnectionListenerManager<AClientConnectionListener, Plugin>
{
	private HashMap<Plugin, LinkedList<AClientConnectionListener>> ownerToListeners = new HashMap<>();

	@Override
	public synchronized void notifyConnected(Object... args)
	{
		validate(args);
		String clientName = (String) args[0];
		ownerToListeners.values().forEach((listeners) ->
		{
			listeners.forEach((listener) ->
			{
				listener.onClientConnected(clientName);
			});
		});
	}

	@Override
	public synchronized void notifyConnected(Plugin owner, Object... args)
	{
		if(ownerToListeners.containsKey(owner))
		{
			validate(args);
			String clientName = (String) args[0];
			ownerToListeners.get(owner).forEach((listener) ->
			{
				listener.onClientConnected(clientName);
			});
		}
	}

	@Override
	public synchronized void notifyDisconnected(Object... args)
	{
		validate(args);
		String clientName = (String) args[0];
		ownerToListeners.values().forEach((listeners) ->
		{
			listeners.forEach((listener) ->
			{
				listener.onClientDisconnected(clientName);
			});
		});
	}

	@Override
	public synchronized void notifyDisconnected(Plugin owner, Object... args)
	{
		if(ownerToListeners.containsKey(owner))
		{
			validate(args);
			String clientName = (String) args[0];
			ownerToListeners.get(owner).forEach((listener) ->
			{
				listener.onClientDisconnected(clientName);
			});
		}
	}

	@Override
	public synchronized void notifyReconnected(Object... args)
	{
	}

	@Override
	public synchronized void notifyReconnected(Plugin owner, Object... args)
	{
	}

	@Override
	public synchronized void registerListener(Plugin owner, AClientConnectionListener listener)
	{
		if(!ownerToListeners.containsKey(owner))
		{
			ownerToListeners.put(owner, new LinkedList<>());
		}
		ownerToListeners.get(owner).add(listener);
	}

	@Override
	public synchronized void unregisterAllListeners()
	{
		ownerToListeners.clear();
	}

	@Override
	public synchronized void unregisterListener(Plugin owner, AClientConnectionListener listener)
	{
		if(ownerToListeners.containsKey(owner))
		{
			ownerToListeners.get(owner).remove(listener);
		}
	}

	@Override
	public synchronized void unregisterListeners(Plugin owner)
	{
		if(ownerToListeners.containsKey(owner))
		{
			ownerToListeners.remove(owner);
		}
	}

	private synchronized void validate(Object... args)
	{
		if(args.length != 1)
		{
			throw new IllegalArgumentException("Arguments array contains more or less than 1 element.");
		}
		if(!(args[0] instanceof String))
		{
			throw new IllegalArgumentException("Argument type is not String");
		}
	}

}
