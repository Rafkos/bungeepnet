/**
 * 
 * Copyright (C) 2018 Rafał Kosyl <admin@rafkos.net>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
*/
package net.rafkos.mc.plugins.BungeePNet.common.listeners;

import java.util.HashMap;
import java.util.LinkedList;

import org.bukkit.plugin.Plugin;

import net.rafkos.mc.plugins.BungeePNet.common.interfaces.IConnectionListenerManager;

public class ServerConnectionListenerManager implements IConnectionListenerManager<AServerConnectionListener, Plugin>
{
	private HashMap<Plugin, LinkedList<AServerConnectionListener>> ownerToListeners = new HashMap<>();

	@Override
	public synchronized void notifyConnected(Plugin owner, Object... args)
	{
		if(ownerToListeners.containsKey(owner))
		{
			ownerToListeners.get(owner).forEach((listener) ->
			{
				listener.onServerConnected();
			});
		}
	}

	@Override
	public synchronized void notifyConnected(Object... args)
	{
		ownerToListeners.values().forEach((listeners) ->
		{
			listeners.forEach((listener) ->
			{
				listener.onServerConnected();
			});
		});
	}

	@Override
	public synchronized void notifyDisconnected(Plugin owner, Object... args)
	{
		if(ownerToListeners.containsKey(owner))
		{
			ownerToListeners.get(owner).forEach((listener) ->
			{
				listener.onServerDisconnected();
			});
		}
	}

	@Override
	public synchronized void notifyDisconnected(Object... args)
	{
		ownerToListeners.values().forEach((listeners) ->
		{
			listeners.forEach((listener) ->
			{
				listener.onServerDisconnected();
			});
		});
	}

	@Override
	public synchronized void notifyReconnected(Plugin owner, Object... args)
	{
		if(ownerToListeners.containsKey(owner))
		{
			ownerToListeners.get(owner).forEach((listener) ->
			{
				listener.onServerReconnected();
			});
		}
	}

	@Override
	public synchronized void notifyReconnected(Object... args)
	{
		ownerToListeners.values().forEach((listeners) ->
		{
			listeners.forEach((listener) ->
			{
				listener.onServerReconnected();
			});
		});
	}

	@Override
	public synchronized void registerListener(Plugin owner, AServerConnectionListener listener)
	{
		if(!ownerToListeners.containsKey(owner))
		{
			ownerToListeners.put(owner, new LinkedList<>());
		}
		ownerToListeners.get(owner).add(listener);
	}

	@Override
	public synchronized void unregisterAllListeners()
	{
		ownerToListeners.clear();
	}

	@Override
	public synchronized void unregisterListener(Plugin owner, AServerConnectionListener listener)
	{
		if(ownerToListeners.containsKey(owner))
		{
			ownerToListeners.get(owner).remove(listener);
		}
	}

	@Override
	public synchronized void unregisterListeners(Plugin owner)
	{
		if(ownerToListeners.containsKey(owner))
		{
			ownerToListeners.remove(owner);
		}
	}

}
