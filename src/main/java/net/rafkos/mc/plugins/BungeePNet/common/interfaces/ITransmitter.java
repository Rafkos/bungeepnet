/**
 * 
 * Copyright (C) 2018 Rafał Kosyl <admin@rafkos.net>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
*/
package net.rafkos.mc.plugins.BungeePNet.common.interfaces;

import net.rafkos.mc.plugins.BungeePNet.common.Callback;
import net.rafkos.mc.plugins.BungeePNet.common.exceptions.ConnectionFailureException;

public interface ITransmitter
{
	public void send(ITarget target, Class<? extends IAction> actionClass, Object[] actionParameters, long timeout,
			Callback callback) throws ConnectionFailureException;

	public void send(ITarget target, Class<? extends IAction> actionClass, Object[] actionParameters, long timeout)
			throws ConnectionFailureException;

	public void send(ITarget target, Class<? extends IAction> actionClass, Object[] actionParameters, Callback callback)
			throws ConnectionFailureException;

	public void send(ITarget target, Class<? extends IAction> actionClass, Object[] actionParameters)
			throws ConnectionFailureException;

}
