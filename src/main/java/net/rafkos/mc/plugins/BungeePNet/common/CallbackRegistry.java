/**
 * 
 * Copyright (C) 2018 Rafał Kosyl <admin@rafkos.net>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
*/
package net.rafkos.mc.plugins.BungeePNet.common;

import java.util.HashMap;
import java.util.UUID;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import net.rafkos.mc.plugins.BungeePNet.common.interfaces.IRegistry;
import net.rafkos.mc.plugins.BungeePNet.common.interfaces.IRegistryEntry;

@Getter
@Setter
public class CallbackRegistry implements IRegistry<UUID, Callback>
{
	private ScheduledExecutorService scheduler = Executors
			.newScheduledThreadPool(Config.DEFAULT_SIZE_OF_TIMEOUT_EXECUTOR_THREAD_POOL);

	@AllArgsConstructor
	@Getter
	private class CallbackRegistryEntry implements IRegistryEntry<Callback>
	{
		private Callback value;
		private long registrationTime, timeout;
	}

	private HashMap<UUID, CallbackRegistryEntry> map = new HashMap<>();

	@Override
	public synchronized boolean contains(UUID uniqueID)
	{
		return this.map.containsKey(uniqueID);
	}

	@Override
	public synchronized boolean register(UUID uniqueId, Callback callback, long timeout)
	{
		boolean flag = false;
		if(!this.map.containsKey(uniqueId))
		{
			this.map.put(uniqueId, new CallbackRegistryEntry(callback, System.currentTimeMillis(), timeout));
			flag = true;

			// schedule timeout task
			scheduler.schedule(new CallbackTimeoutTask(uniqueId, this), timeout, TimeUnit.MILLISECONDS);
		}
		return flag;
	}

	@Override
	public synchronized IRegistryEntry<Callback> unregister(UUID uniqueId)
	{

		IRegistryEntry<Callback> entry = null;
		if(this.map.containsKey(uniqueId))
		{
			entry = this.map.remove(uniqueId);
		}
		return entry;
	}

	@Override
	public synchronized boolean isEmpty()
	{
		return map.isEmpty();
	}

}
