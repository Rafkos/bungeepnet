/**
 * 
 * Copyright (C) 2018 Rafał Kosyl <admin@rafkos.net>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
*/
package net.rafkos.mc.plugins.BungeePNet.common;

import java.util.UUID;

import net.rafkos.mc.plugins.BungeePNet.common.enums.ETargetType;
import net.rafkos.mc.plugins.BungeePNet.common.interfaces.ITarget;
import net.rafkos.mc.plugins.BungeePNet.common.targets.TargetBungeeServer;
import net.rafkos.mc.plugins.BungeePNet.common.targets.TargetClientName;
import net.rafkos.mc.plugins.BungeePNet.common.targets.TargetPlayerName;
import net.rafkos.mc.plugins.BungeePNet.common.targets.TargetPlayerUniqueId;

public class TargetBuilder
{
	public static ITarget getTarget(ETargetType type, String parameter)
	{
		ITarget target = null;
		switch(type)
		{
		case BUNGEE_SERVER:
			target = new TargetBungeeServer();
			break;
		case CLIENT_NAME:
			target = new TargetClientName(parameter);
			break;
		case PLAYER_NAME:
			target = new TargetPlayerName(parameter);
			break;
		case PLAYER_UUID:
			target = new TargetPlayerUniqueId(UUID.fromString(parameter));
			break;
		default:
			throw new IllegalArgumentException(type.toString());
		}
		return target;
	}
}
