/**
 * 
 * Copyright (C) 2018 Rafał Kosyl <admin@rafkos.net>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
*/
package net.rafkos.mc.plugins.BungeePNet.common.enums;

import java.io.Serializable;

public enum EMessageStatus implements Serializable
{
	SUCCESS, FAILED;
	
	public int getId()
	{
		switch(this)
		{
		case SUCCESS:
			return 1;
		case FAILED:
			return 0;
		default:
			throw new IllegalArgumentException();
		}
	}
	
	public static EMessageStatus fromId(int id)
	{
		switch(id)
		{
		case 0:
			return FAILED;
		case 1:
			return SUCCESS;
		default:
			throw new IllegalArgumentException();
		}
	}
	
}
