/**
 * 
 * Copyright (C) 2018 Rafał Kosyl <admin@rafkos.net>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
*/
package net.rafkos.mc.plugins.BungeePNet.common.interfaces;

public interface IConnectionListenerManager<T, K>
{
	public void notifyConnected(Object... args);

	public void notifyReconnected(Object... args);

	public void notifyDisconnected(Object... args);

	public void notifyConnected(K owner, Object... args);

	public void notifyReconnected(K owner, Object... args);

	public void notifyDisconnected(K owner, Object... args);

	public void registerListener(K owner, T listener);

	public void unregisterAllListeners();

	public void unregisterListeners(K owner);

	public void unregisterListener(K owner, T listener);
}
