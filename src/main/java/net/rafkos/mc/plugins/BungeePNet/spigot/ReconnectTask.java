/**
 * 
 * Copyright (C) 2018 Rafał Kosyl <admin@rafkos.net>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
*/
package net.rafkos.mc.plugins.BungeePNet.spigot;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import org.bukkit.Bukkit;

import lombok.extern.log4j.Log4j;

@Log4j
public class ReconnectTask implements Runnable
{
	private static AtomicBoolean disable = new AtomicBoolean(false);
	private AtomicInteger reconnectAttempts = new AtomicInteger(0);

	public ReconnectTask(int reconnectAttempts)
	{
		this.reconnectAttempts.set(reconnectAttempts);
	}

	public synchronized static boolean isEnabled()
	{
		return !disable.get();
	}

	public synchronized static void setDisabled(boolean flag)
	{
		disable.set(flag);
	}

	public ReconnectTask()
	{
		if(!disable.get())
		{
			log.info("Reconnection task started");
		}
	}

	@Override
	public void run()
	{
		int counter = reconnectAttempts.incrementAndGet();
		if(!disable.get())
		{
			log.info("Attempting to reconnect... [" + counter + "/"
					+ SpigotApi.getClientConfig().getReconnectionAttemptsLimit() + "]");
			// reconnect
			if(!SpigotApi.getClient().connect())
			{
				// schedule another reconnect task
				if(SpigotApi.getClientConfig().getReconnectionAttemptsLimit() > counter
						|| SpigotApi.getClientConfig().getReconnectionAttemptsLimit() <= 0)
				{
					SpigotApi.getExecutorService().schedule(new ReconnectTask(counter),
							SpigotApi.getClientConfig().getReconnectIntervalMilliseconds(), TimeUnit.MILLISECONDS);
				}
				// attempts limit reached, close Spigot if connection required
				else if(SpigotApi.getClientConfig().getReconnectionAttemptsLimit() == counter
						&& SpigotApi.getClientConfig().isConnectionToServerRequired())
				{
					Bukkit.shutdown();
				}

			}
		}
	}

}
