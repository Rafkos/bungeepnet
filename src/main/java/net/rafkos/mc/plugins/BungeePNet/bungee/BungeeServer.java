/**
 * 
 * Copyright (C) 2018 Rafał Kosyl <admin@rafkos.net>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
*/
package net.rafkos.mc.plugins.BungeePNet.bungee;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import lombok.extern.log4j.Log4j;
import net.md_5.bungee.api.ProxyServer;
import net.rafkos.mc.plugins.BungeePNet.bungee.listeners.BungeeServerListener;
import net.rafkos.mc.plugins.BungeePNet.common.interfaces.IServer;
import nl.pvdberg.pnet.server.Server;
import nl.pvdberg.pnet.server.util.PlainServer;
import nl.pvdberg.pnet.server.util.TLSServer;

@Log4j
public class BungeeServer implements IServer
{

	private int port;
	private Server server;

	public BungeeServer(int port)
	{
		this.port = port;
		init();
	}

	private synchronized void init()
	{

		log.info("Starting server at port: " + BungeeApi.getServerConfig().getHostPort());
		(new Thread()
		{
			@Override
			public void run()
			{
				if(BungeeApi.getServerConfig().isTLSEnabled())
				{
					File keyStore = new File(BungeeApi.getPlugin().getDataFolder().getPath() + "/server/"
							+ BungeeApi.getServerConfig().getKeyStoreFileName());
					try
					{
						byte[] keyStoreBytes = Files.readAllBytes(keyStore.toPath());
						BungeeServer.this.server = new TLSServer(keyStoreBytes,
								BungeeApi.getServerConfig().getKeyStorePassword().toCharArray(),
								BungeeApi.getServerConfig().getKeyStoreType());
					}catch(IOException e)
					{
						log.error("Unable to setup server and TLS connection, exiting... " + e.getLocalizedMessage());
						ProxyServer.getInstance().stop();
					}
				}else
				{
					try
					{
						BungeeServer.this.server = new PlainServer();
					}catch(IOException e)
					{
						log.info("Unable to start server: " + e.getLocalizedMessage());
						ProxyServer.getInstance().stop();
					}
				}
				BungeeServer.this.server.setListener(new BungeeServerListener());
				BungeeServer.this.server.start(BungeeServer.this.port);
			}
		}).start();
	}

	@Override
	public synchronized boolean isRunning()
	{
		return this.server != null;
	}

	@Override
	public synchronized void stop()
	{
		this.server.stop();
	}

}
