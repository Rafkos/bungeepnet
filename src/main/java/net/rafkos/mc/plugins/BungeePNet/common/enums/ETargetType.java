/**
 * 
 * Copyright (C) 2018 Rafał Kosyl <admin@rafkos.net>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
*/
package net.rafkos.mc.plugins.BungeePNet.common.enums;

import java.io.Serializable;

public enum ETargetType implements Serializable
{
	PLAYER_UUID, PLAYER_NAME, CLIENT_NAME, BUNGEE_SERVER;
	
	public int getId()
	{
		switch(this)
		{
		case BUNGEE_SERVER:
			return 3;
		case CLIENT_NAME:
			return 2;
		case PLAYER_NAME:
			return 1;
		case PLAYER_UUID:
			return 0;
		default:
			throw new IllegalArgumentException();
		}
	}
	
	public static ETargetType fromId(int id)
	{
		switch(id)
		{
		case 0:
			return PLAYER_UUID;
		case 1:
			return PLAYER_NAME;
		case 2:
			return CLIENT_NAME;
		case 3:
			return BUNGEE_SERVER;
		default:
			throw new IllegalArgumentException();
		}
	}
}
