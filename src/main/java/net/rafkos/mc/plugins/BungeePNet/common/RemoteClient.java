/**
 * 
 * Copyright (C) 2018 Rafał Kosyl <admin@rafkos.net>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
*/
package net.rafkos.mc.plugins.BungeePNet.common;

import java.net.Socket;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import net.rafkos.mc.plugins.BungeePNet.common.interfaces.IClientSender;
import nl.pvdberg.pnet.client.Client;
import nl.pvdberg.pnet.packet.Packet;

@AllArgsConstructor
@Getter
@Setter
public class RemoteClient implements IClientSender
{
	private Client client;

	@Override
	public boolean send(Packet p)
	{
		return client.send(p);
	}

	@Override
	public boolean equals(Object o)
	{
		if(!(o instanceof RemoteClient))
		{
			return false;
		}
		Socket other = ((RemoteClient) o).getClient().getSocket();
		return client.getSocket().getPort() == other.getPort()
				&& other.getInetAddress().getAddress().equals(client.getInetAddress().getAddress());
	}

	@Override
	public String toString()
	{
		return client.toString();
	}

	@Override
	public int hashCode()
	{
		return client.getSocket().hashCode();
	}

}
