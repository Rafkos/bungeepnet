/**
 * 
 * Copyright (C) 2018 Rafał Kosyl <admin@rafkos.net>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
*/
package net.rafkos.mc.plugins.BungeePNet.bungee;

import java.util.UUID;

import net.rafkos.mc.plugins.BungeePNet.common.Callback;
import net.rafkos.mc.plugins.BungeePNet.common.Config;
import net.rafkos.mc.plugins.BungeePNet.common.LocalClient;
import net.rafkos.mc.plugins.BungeePNet.common.PackagingGnome;
import net.rafkos.mc.plugins.BungeePNet.common.enums.ETargetType;
import net.rafkos.mc.plugins.BungeePNet.common.exceptions.ConnectionFailureException;
import net.rafkos.mc.plugins.BungeePNet.common.interfaces.IAction;
import net.rafkos.mc.plugins.BungeePNet.common.interfaces.ITarget;
import net.rafkos.mc.plugins.BungeePNet.common.interfaces.ITransmitter;
import net.rafkos.mc.plugins.BungeePNet.common.targets.TargetBungeeServer;
import nl.pvdberg.pnet.packet.Packet.PacketType;
import nl.pvdberg.pnet.packet.PacketBuilder;

public class ActionTransmitter implements ITransmitter
{
	private static final LocalClient localClient = new LocalClient();

	@Override
	public void send(ITarget target, Class<? extends IAction> actionClass, Object[] actionParameters, long timeout,
			Callback callback) throws ConnectionFailureException
	{

		if(target instanceof TargetBungeeServer)
		{
			throw new IllegalArgumentException("Unable to send a message from server to server.");
		}
		
		if(!BungeeApi.getServer().isRunning())
		{
			throw new ConnectionFailureException();
		}

		UUID callbackUniqueId = UUID.randomUUID();
		if(BungeeApi.getCallbackRegistry().register(callbackUniqueId, callback, timeout))
		{
			PacketBuilder packetBuilder = new PacketBuilder(PacketType.Request);
			packetBuilder.withID(Config.REQUEST_PACKET_ID);
			packetBuilder.withInt(target.getType().getId());
			packetBuilder.withString(target.getValue());
			packetBuilder.withInt(ETargetType.BUNGEE_SERVER.getId());
			packetBuilder.withString("");
			packetBuilder.withString(callbackUniqueId.toString());
			packetBuilder.withString(actionClass.getName());
			packetBuilder.withBytes(PackagingGnome.packObject(actionParameters));
			
			localClient.send(packetBuilder.build());

		}else
		{
			throw new IllegalArgumentException("Unable to register callback: " + callback + ".");
		}

	}

	@Override
	public void send(ITarget target, Class<? extends IAction> actionClass, Object[] actionParameters, Callback callback)
			throws ConnectionFailureException
	{
		send(target, actionClass, actionParameters, Config.DEFAULT_TIMEOUT_MILLIS, callback);
	}

	@Override
	public void send(ITarget target, Class<? extends IAction> actionClass, Object[] actionParameters, long timeout)
			throws ConnectionFailureException
	{
		send(target, actionClass, actionParameters, timeout, new Callback());
	}

	@Override
	public void send(ITarget target, Class<? extends IAction> actionClass, Object[] actionParameters)
			throws ConnectionFailureException
	{
		send(target, actionClass, actionParameters, Config.DEFAULT_TIMEOUT_MILLIS, new Callback());
	}

}
