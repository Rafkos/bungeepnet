/**
 * 
 * Copyright (C) 2018 Rafał Kosyl <admin@rafkos.net>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
*/
package net.rafkos.mc.plugins.BungeePNet.bungee;

import java.util.HashMap;
import java.util.Iterator;

import net.rafkos.mc.plugins.BungeePNet.common.Config;
import net.rafkos.mc.plugins.BungeePNet.common.HeartbeatMonitor;
import nl.pvdberg.pnet.client.Client;

public class ClientHeartbeatMonitorRegistry
{
	private HashMap<Client, HeartbeatMonitor> monitors = new HashMap<>();
	
	public synchronized void registerClient(Client c)
	{
		monitors.put(c, new HeartbeatMonitor(c, Config.HEARTBEAT_MONITOR_MILLIS, BungeeApi.getExecutorService()));
	}
	
	public synchronized void unregisterClient(Client c)
	{
		HeartbeatMonitor monitor = null;
		if((monitor = monitors.remove(c)) != null)
		{
			monitor.terminate();
		}
	}
	
	public synchronized void unregisterAll()
	{
		Iterator<Client> it = monitors.keySet().iterator();
		while(it.hasNext())
		{
			unregisterClient(it.next());
		}
	}
	
	public synchronized HeartbeatMonitor getHeartbeatMonitor(Client c)
	{
		return monitors.get(c);
	}
	
}
