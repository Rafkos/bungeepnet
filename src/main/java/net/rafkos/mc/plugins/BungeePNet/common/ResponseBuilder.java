/**
 * 
 * Copyright (C) 2018 Rafał Kosyl <admin@rafkos.net>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
*/
package net.rafkos.mc.plugins.BungeePNet.common;

import java.util.HashMap;

import lombok.SneakyThrows;
import net.rafkos.mc.plugins.BungeePNet.common.enums.EMessageStatus;
import net.rafkos.mc.plugins.BungeePNet.common.interfaces.IAction;
import nl.pvdberg.pnet.packet.Packet;
import nl.pvdberg.pnet.packet.Packet.PacketType;
import nl.pvdberg.pnet.packet.PacketBuilder;
import nl.pvdberg.pnet.packet.PacketReader;

public class ResponseBuilder
{
	/**
	 * Request packet reader without first two objects (int targetTypeId, String targetValue).
	 */
	@SneakyThrows
	public static Packet executeRequest(PacketReader reader)
	{
		PacketBuilder packetBuilder = new PacketBuilder(PacketType.Reply);
		packetBuilder.withID(Config.RESPONSE_PACKET_ID);
		
		packetBuilder.withInt(reader.readInt()); // source type ID
		packetBuilder.withString(reader.readString()); // source value
		packetBuilder.withString(reader.readString()); // callback UUID
		
		IAction action = ActionBuilder.getAction(reader.readString(), (Object[]) PackagingGnome.unpackObject(reader.readBytes()));
		HashMap<String, Object> result = null;
		
		try
		{
			result = action.execute();
			packetBuilder.withInt(EMessageStatus.SUCCESS.getId()); // status id
			try 
			{
				packetBuilder.withBytes(PackagingGnome.packObject(result)); // results map
			}
			catch(Exception e)
			{
				throw new IllegalArgumentException("Illegal arguments inside results map");
			}
			packetBuilder.withBytes(new byte[0]); // exception
		}catch(Exception e)
		{
			packetBuilder.withInt(EMessageStatus.FAILED.getId());
			packetBuilder.withBytes(new byte[0]); // results map
			packetBuilder.withBytes(PackagingGnome.packObject(e)); // exception
		}
		
		return packetBuilder.build();
	}

	/**
	 * Request packet reader without first two objects (int targetTypeId, String targetValue).
	 */
	@SneakyThrows
	public static Packet respondWithError(PacketReader reader, Throwable e)
	{
		PacketBuilder packetBuilder = new PacketBuilder(PacketType.Reply);
		packetBuilder.withID(Config.RESPONSE_PACKET_ID);
		
		packetBuilder.withInt(reader.readInt()); // target type ID
		packetBuilder.withString(reader.readString()); // target value
		packetBuilder.withString(reader.readString()); // callback UUID
		packetBuilder.withInt(EMessageStatus.FAILED.getId());
		packetBuilder.withBytes(new byte[0]); // results map
		packetBuilder.withBytes(PackagingGnome.packObject(e)); // exception

		return packetBuilder.build();
	}
}
