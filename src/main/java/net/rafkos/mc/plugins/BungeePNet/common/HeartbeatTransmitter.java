/**
 * 
 * Copyright (C) 2018 Rafał Kosyl <admin@rafkos.net>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
*/
package net.rafkos.mc.plugins.BungeePNet.common;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j;
import nl.pvdberg.pnet.client.Client;
import nl.pvdberg.pnet.packet.Packet;
import nl.pvdberg.pnet.packet.Packet.PacketType;

@Log4j
public class HeartbeatTransmitter
{
	private Client client;
	private long intervalMillis;
	private ScheduledExecutorService executor;
	private AtomicBoolean active = new AtomicBoolean(true);
	
	public HeartbeatTransmitter(Client client, long intervalMillis, ScheduledExecutorService executor)
	{
		this.client = client;
		this.intervalMillis = intervalMillis;
		this.executor = executor;
		startTransmitter();
	}

	private void startTransmitter()
	{
		log.info("Starting a heartbeat transmitter for "+client.toString());
		executor.schedule(new TransmitterTask(), intervalMillis, TimeUnit.MILLISECONDS);
	}
	
	public void terminate()
	{
		active.set(false);
	}
	
	private class TransmitterTask implements Runnable
	{

		@Override
		@SneakyThrows
		public void run()
		{
			mainLoop:
			while(active.get())
			{
				// send poke to client
				if(client.isConnected())
				{
					client.send(new Packet(PacketType.Request, Config.HEARTBEAT_PACKET_ID, new byte[0]));
				}
				else
				{
					log.info("Heartbeat transmitter has stopped for "+client.toString());
					active.set(false);
				}
				
				for(int i = 0; i < intervalMillis/10; i++)
				{
					if(!active.get())
					{
						log.info("Terminating heartbeat transmitter for "+client.toString());
						break mainLoop;
					}
					Thread.sleep(10);
				}
				
			}
		}
		
	}
	
}
