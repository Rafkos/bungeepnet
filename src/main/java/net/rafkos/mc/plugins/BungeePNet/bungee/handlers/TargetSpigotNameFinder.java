/**
 * 
 * Copyright (C) 2018 Rafał Kosyl <admin@rafkos.net>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
*/
package net.rafkos.mc.plugins.BungeePNet.bungee.handlers;

import java.util.UUID;

import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.rafkos.mc.plugins.BungeePNet.bungee.BungeeApi;

public class TargetSpigotNameFinder
{

	public static String getSpigotNameOfPlayerName(String playerName)
	{
		ProxiedPlayer pp = BungeeApi.getPlugin().getProxy().getPlayer(playerName);
		String serverName = null;
		if(pp != null)
		{
			try
			{
				serverName = pp.getServer().getInfo().getName();
			}catch(NullPointerException e)
			{
				serverName = null;
			}
		}

		return serverName;
	}

	public static String getSpigotNameOfPlayerUniqueId(UUID playerUniqueid)
	{
		ProxiedPlayer pp = BungeeApi.getPlugin().getProxy().getPlayer(playerUniqueid);
		String serverName = null;
		if(pp != null)
		{
			try
			{
				serverName = pp.getServer().getInfo().getName();
			}catch(NullPointerException e)
			{
				serverName = null;
			}
		}
		return serverName;
	}

}
