/**
 * 
 * Copyright (C) 2018 Rafał Kosyl <admin@rafkos.net>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
*/

package net.rafkos.mc.plugins.BungeePNet.bungee.handlers.redirectors;

import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j;
import net.rafkos.mc.plugins.BungeePNet.bungee.BungeeApi;
import net.rafkos.mc.plugins.BungeePNet.common.ResponseBuilder;
import net.rafkos.mc.plugins.BungeePNet.common.exceptions.ConnectionFailureException;
import net.rafkos.mc.plugins.BungeePNet.common.interfaces.IClientSender;
import net.rafkos.mc.plugins.BungeePNet.common.interfaces.IMessageRedirectHandler;
import nl.pvdberg.pnet.client.Client;
import nl.pvdberg.pnet.packet.Packet;
import nl.pvdberg.pnet.packet.PacketReader;

@Log4j
public class ResponseClientNameRedirectHandler implements IMessageRedirectHandler
{

	@Override
	@SneakyThrows
	public void redirect(PacketReader reader, IClientSender source, Packet p)
	{
		String serverName = reader.readString();
		Client t = null;

		if((t = BungeeApi.getClientNameRegistry().getClientByName(serverName)) != null)
		{
			if(!t.send(p))
			{
				log.error("A communication failure has occured while sending a request to "+serverName);
				Packet response = ResponseBuilder.respondWithError(reader, new ConnectionFailureException());
				if(!source.send(response))
				{
					log.error("A communication failure has occured while sending a response to "+source);
				}
			}
		}else
		{
			log.warn("Unable to redirect response to " + serverName + ". Client not connected?");
		}
	}

}
