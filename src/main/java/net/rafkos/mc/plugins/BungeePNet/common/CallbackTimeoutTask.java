/**
 * 
 * Copyright (C) 2018 Rafał Kosyl <admin@rafkos.net>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
*/
package net.rafkos.mc.plugins.BungeePNet.common;

import java.util.TimerTask;
import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import net.rafkos.mc.plugins.BungeePNet.common.exceptions.ConnectionTimeoutException;
import net.rafkos.mc.plugins.BungeePNet.common.interfaces.IRegistryEntry;

@AllArgsConstructor
@Getter
@Setter
public class CallbackTimeoutTask extends TimerTask
{
	private UUID callbackUniqueId;
	private CallbackRegistry callbackRegistry;

	@Override
	public void run()
	{
		IRegistryEntry<Callback> entry = null;

		// if callback is still registered
		if((entry = callbackRegistry.unregister(getCallbackUniqueId())) != null)
		{
			entry.getValue().getErrorHandler().execute(new ConnectionTimeoutException());
		}

	}

}