/**
 * 
 * Copyright (C) 2018 Rafał Kosyl <admin@rafkos.net>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
*/
package net.rafkos.mc.plugins.BungeePNet.spigot.handlers.packets;

import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j;
import net.rafkos.mc.plugins.BungeePNet.common.enums.ETargetType;
import net.rafkos.mc.plugins.BungeePNet.common.interfaces.IClientSender;
import net.rafkos.mc.plugins.BungeePNet.common.interfaces.IMessageRedirectHandler;
import net.rafkos.mc.plugins.BungeePNet.common.interfaces.IPacketHandler;
import net.rafkos.mc.plugins.BungeePNet.spigot.SpigotApi;
import nl.pvdberg.pnet.packet.Packet;
import nl.pvdberg.pnet.packet.PacketReader;

@Log4j
public class RequestHandler implements IPacketHandler
{

	@SneakyThrows
	@Override
	public void handlePacket(Packet p, IClientSender c)
	{
		
		PacketReader reader = new PacketReader(p);
		ETargetType targetType = ETargetType.fromId(reader.readInt());

		IMessageRedirectHandler handler = SpigotApi.getRequestRedirectManager().getHandler(targetType);
		if(handler != null)
		{
			handler.redirect(reader, c, p);
		}else
		{
			log.warn("Received a request of unsupported type " + targetType + ", ignoring");
		}

	}

}
