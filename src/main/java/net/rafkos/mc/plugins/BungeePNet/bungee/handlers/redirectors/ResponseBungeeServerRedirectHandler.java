/**
 * 
 * Copyright (C) 2018 Rafał Kosyl <admin@rafkos.net>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
*/
package net.rafkos.mc.plugins.BungeePNet.bungee.handlers.redirectors;

import java.util.HashMap;
import java.util.UUID;

import lombok.SneakyThrows;
import net.rafkos.mc.plugins.BungeePNet.bungee.BungeeApi;
import net.rafkos.mc.plugins.BungeePNet.common.Callback;
import net.rafkos.mc.plugins.BungeePNet.common.PackagingGnome;
import net.rafkos.mc.plugins.BungeePNet.common.enums.EMessageStatus;
import net.rafkos.mc.plugins.BungeePNet.common.interfaces.IClientSender;
import net.rafkos.mc.plugins.BungeePNet.common.interfaces.IMessageRedirectHandler;
import net.rafkos.mc.plugins.BungeePNet.common.interfaces.IRegistryEntry;
import nl.pvdberg.pnet.packet.Packet;
import nl.pvdberg.pnet.packet.PacketReader;

public class ResponseBungeeServerRedirectHandler implements IMessageRedirectHandler
{

	@SuppressWarnings("unchecked")
	@Override
	@SneakyThrows
	public void redirect(PacketReader reader, IClientSender source, Packet p)
	{
		reader.readString(); // no target value for bungee server

		UUID callbackUniqueId = UUID.fromString(reader.readString());
		IRegistryEntry<Callback> registryEntry = null;

		if((registryEntry = BungeeApi.getCallbackRegistry().unregister(callbackUniqueId)) != null)
		{
			Callback callback = registryEntry.getValue();
			switch(EMessageStatus.fromId(reader.readInt()))
			{
			case SUCCESS:
				callback.getResultHandler()
						.execute((HashMap<String, Object>) PackagingGnome.unpackObject(reader.readBytes()));
				break;
			case FAILED:
				reader.readBytes(); // ignore results map
				callback.getErrorHandler()
						.execute((Throwable) PackagingGnome.unpackObject(reader.readBytes()));
				break;
			}

		}
	}

}
