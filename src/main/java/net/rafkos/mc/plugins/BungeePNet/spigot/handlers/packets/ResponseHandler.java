/**
 * 
 * Copyright (C) 2018 Rafał Kosyl <admin@rafkos.net>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
*/
package net.rafkos.mc.plugins.BungeePNet.spigot.handlers.packets;

import java.util.HashMap;
import java.util.UUID;

import lombok.SneakyThrows;
import net.rafkos.mc.plugins.BungeePNet.common.Callback;
import net.rafkos.mc.plugins.BungeePNet.common.PackagingGnome;
import net.rafkos.mc.plugins.BungeePNet.common.enums.EMessageStatus;
import net.rafkos.mc.plugins.BungeePNet.common.interfaces.IRegistryEntry;
import net.rafkos.mc.plugins.BungeePNet.spigot.SpigotApi;
import nl.pvdberg.pnet.client.Client;
import nl.pvdberg.pnet.event.PacketHandler;
import nl.pvdberg.pnet.packet.Packet;
import nl.pvdberg.pnet.packet.PacketReader;

public class ResponseHandler implements PacketHandler
{

	@SuppressWarnings("unchecked")
	@SneakyThrows
	@Override
	public void handlePacket(Packet p, Client c)
	{

		PacketReader reader = new PacketReader(p);
		reader.readInt(); // ignore target type id
		reader.readString(); // ignore target value

		UUID callbackUniqueId = UUID.fromString(reader.readString());

		final IRegistryEntry<Callback> registryEntry;
		if((registryEntry = SpigotApi.getCallbackRegistry().unregister(callbackUniqueId)) != null)
		{
			Callback callback = registryEntry.getValue();
			switch(EMessageStatus.fromId(reader.readInt()))
			{
			case SUCCESS:
				callback.getResultHandler()
						.execute((HashMap<String, Object>) PackagingGnome.unpackObject(reader.readBytes()));
				break;
			case FAILED:
				reader.readBytes(); // ignore results map
				callback.getErrorHandler()
						.execute((Throwable) PackagingGnome.unpackObject(reader.readBytes()));
				break;
			}

		}

	}

}
