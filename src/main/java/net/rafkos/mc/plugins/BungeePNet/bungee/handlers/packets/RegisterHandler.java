/**
 * 
 * Copyright (C) 2018 Rafał Kosyl <admin@rafkos.net>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
*/
package net.rafkos.mc.plugins.BungeePNet.bungee.handlers.packets;

import lombok.SneakyThrows;
import net.rafkos.mc.plugins.BungeePNet.bungee.BungeeServerFunctions;
import net.rafkos.mc.plugins.BungeePNet.common.RemoteClient;
import net.rafkos.mc.plugins.BungeePNet.common.interfaces.IClientSender;
import net.rafkos.mc.plugins.BungeePNet.common.interfaces.IPacketHandler;
import nl.pvdberg.pnet.packet.Packet;

public class RegisterHandler implements IPacketHandler
{
	@SneakyThrows
	@Override
	public void handlePacket(Packet p, IClientSender c)
	{
		String name = new String(p.getData());
		BungeeServerFunctions.registerClient(((RemoteClient) c).getClient(), name);
	}

}
