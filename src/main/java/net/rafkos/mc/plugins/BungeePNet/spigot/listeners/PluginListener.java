/**
 * 
 * Copyright (C) 2018 Rafał Kosyl <admin@rafkos.net>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
*/
package net.rafkos.mc.plugins.BungeePNet.spigot.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.PluginDisableEvent;
import org.bukkit.event.server.PluginEnableEvent;

import net.rafkos.mc.plugins.BungeePNet.spigot.SpigotApi;

public class PluginListener implements Listener
{

	/**
	 * Purpose of this method is to notify loaded plugins's listeners if a
	 * connection to Bungee server is present.
	 */
	@EventHandler
	public void onPluginEnabled(PluginEnableEvent e)
	{
		// ignore self
		if(e.getPlugin() != SpigotApi.getPlugin())
		{
			if(SpigotApi.getClient().isConnected())
			{
				SpigotApi.getServerConnectionListenerManager().notifyConnected(e.getPlugin(), new Object());
			}
		}
	}

	/**
	 * Purpose of this method is to unregister disabled plugin's listeners.
	 */
	@EventHandler
	public void onPluginDisabled(PluginDisableEvent e)
	{
		SpigotApi.getServerConnectionListenerManager().unregisterListeners(e.getPlugin());
	}
	
}
