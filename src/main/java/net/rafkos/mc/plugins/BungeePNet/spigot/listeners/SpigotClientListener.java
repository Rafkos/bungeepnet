/**
 * 
 * Copyright (C) 2018 Rafał Kosyl <admin@rafkos.net>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
*/
package net.rafkos.mc.plugins.BungeePNet.spigot.listeners;

import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.atomic.AtomicBoolean;

import org.bukkit.Bukkit;

import lombok.extern.log4j.Log4j;
import net.rafkos.mc.plugins.BungeePNet.common.Config;
import net.rafkos.mc.plugins.BungeePNet.common.RemoteClient;
import net.rafkos.mc.plugins.BungeePNet.spigot.ReconnectTask;
import net.rafkos.mc.plugins.BungeePNet.spigot.SpigotApi;
import net.rafkos.mc.plugins.BungeePNet.spigot.handlers.packets.HeartbeatHandler;
import net.rafkos.mc.plugins.BungeePNet.spigot.handlers.packets.RequestHandler;
import net.rafkos.mc.plugins.BungeePNet.spigot.handlers.packets.ResponseHandler;
import nl.pvdberg.pnet.client.Client;
import nl.pvdberg.pnet.event.PNetListener;
import nl.pvdberg.pnet.packet.Packet;
import nl.pvdberg.pnet.packet.Packet.PacketType;

@Log4j
public class SpigotClientListener implements PNetListener
{
	private RequestHandler requestHandler = new RequestHandler();
	private ResponseHandler responseHandler = new ResponseHandler();
	private HeartbeatHandler heartbeatHandler = new HeartbeatHandler();
	private AtomicBoolean connectedOnce = new AtomicBoolean();

	@Override
	public void onConnect(Client c)
	{
		log.info("Connected to server " + SpigotApi.getClientConfig().getHostIp() + ":"
				+ SpigotApi.getClientConfig().getHostPort());

		Packet registerMePacket = new Packet(PacketType.Request, Config.REGISTER_PACKET_ID,
				SpigotApi.getClientConfig().getClientName().getBytes());

		log.info("Sending a registration as '" + SpigotApi.getClientConfig().getClientName()
				+ "' request to the server.");

		if(!c.send(registerMePacket))
		{
			log.error(
					"A communication error has occured during sending of register request. The client will not work correctly. Please restart.");
		}
		// connected for the first time
		if(!connectedOnce.getAndSet(true))
		{
			SpigotApi.getServerConnectionListenerManager().notifyConnected(new Object());
		}
		// reconnected
		else
		{
			SpigotApi.getServerConnectionListenerManager().notifyReconnected(new Object());
		}

	}

	@Override
	public void onDisconnect(Client c)
	{
		log.warn("Disconnected from server");
		SpigotApi.getClient().getHeartBeatMonitor().terminate();
		SpigotApi.getClient().getHeartBeatTransmitter().terminate();
		SpigotApi.getServerConnectionListenerManager().notifyDisconnected(new Object());

		if(!SpigotApi.getClientConfig().isReconnectEnabled() && SpigotApi.getClientConfig().isConnectionToServerRequired())
		{
			Bukkit.shutdown();
		}
		// start reconnect task
		else if(SpigotApi.getClientConfig().isReconnectEnabled() && ReconnectTask.isEnabled())
		{
			try
			{
				SpigotApi.getExecutorService().execute(new ReconnectTask(0));
			}
			catch(RejectedExecutionException e)
			{
				log.warn("Unable to execute reconnection task.");
			}
		}
	}

	@Override
	public void onReceive(Packet p, Client c)
	{
		short id = p.getPacketID();

		switch(id)
		{
		case Config.REQUEST_PACKET_ID:
			SpigotApi.getPlugin().getServer().getScheduler().scheduleSyncDelayedTask(SpigotApi.getPlugin(), () ->
			{
				this.requestHandler.handlePacket(p, new RemoteClient(c));
			}, 0);
			break;
		case Config.RESPONSE_PACKET_ID:
			SpigotApi.getPlugin().getServer().getScheduler().scheduleSyncDelayedTask(SpigotApi.getPlugin(), () ->
			{
				this.responseHandler.handlePacket(p, c);
			}, 0);
			break;
		case Config.HEARTBEAT_PACKET_ID:
			this.heartbeatHandler.handlePacket(p, c);
			break;
		}
	}

}
