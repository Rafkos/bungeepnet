/**
 * 
 * Copyright (C) 2018 Rafał Kosyl <admin@rafkos.net>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
*/
package net.rafkos.mc.plugins.BungeePNet.bungee.handlers.packets;

import lombok.extern.log4j.Log4j;
import net.rafkos.mc.plugins.BungeePNet.bungee.BungeeApi;
import net.rafkos.mc.plugins.BungeePNet.common.HeartbeatMonitor;
import net.rafkos.mc.plugins.BungeePNet.common.RemoteClient;
import net.rafkos.mc.plugins.BungeePNet.common.interfaces.IClientSender;
import net.rafkos.mc.plugins.BungeePNet.common.interfaces.IPacketHandler;
import nl.pvdberg.pnet.client.Client;
import nl.pvdberg.pnet.packet.Packet;

@Log4j
public class HeartbeatHandler implements IPacketHandler
{
	@Override
	public void handlePacket(Packet p, IClientSender c)
	{
		if(c instanceof RemoteClient)
		{
			HeartbeatMonitor heartbeatMonitor = null;
			Client client = ((RemoteClient) c).getClient();
			if((heartbeatMonitor = BungeeApi.getHeartbeatMonitorRegistry().getHeartbeatMonitor(client)) != null)
			{
				heartbeatMonitor.beat();
			}
		}
		else
		{
			log.warn("Received a heartbeat packet from an unsupported client "+c);
		}
	}

}
