/**
 * 
 * Copyright (C) 2018 Rafał Kosyl <admin@rafkos.net>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
*/
package net.rafkos.mc.plugins.BungeePNet.spigot.handlers.packets.redirectors;

import java.util.HashMap;

import net.rafkos.mc.plugins.BungeePNet.common.enums.ETargetType;
import net.rafkos.mc.plugins.BungeePNet.common.interfaces.IMessageRedirectHandler;
import net.rafkos.mc.plugins.BungeePNet.common.interfaces.IMessageRedirectManager;

public class RequestRedirectManager implements IMessageRedirectManager
{
	private HashMap<ETargetType, IMessageRedirectHandler> redirectHandlers = new HashMap<>();

	public RequestRedirectManager()
	{
		this.redirectHandlers.put(ETargetType.CLIENT_NAME, new RequestClientNameRedirectHandler());
		this.redirectHandlers.put(ETargetType.PLAYER_UUID, new RequestPlayerUniqueIdRedirectHandler());
		this.redirectHandlers.put(ETargetType.PLAYER_NAME, new RequestPlayerNameRedirectHandler());
	}

	@Override
	public IMessageRedirectHandler getHandler(ETargetType type)
	{
		if(!this.redirectHandlers.containsKey(type))
		{
			return null;
		}
		return this.redirectHandlers.get(type);
	}

}
