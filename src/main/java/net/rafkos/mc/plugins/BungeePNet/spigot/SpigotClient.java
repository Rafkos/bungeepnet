/**
 * 
 * Copyright (C) 2018 Rafał Kosyl <admin@rafkos.net>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
*/
package net.rafkos.mc.plugins.BungeePNet.spigot;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.concurrent.RejectedExecutionException;

import org.bukkit.Bukkit;

import lombok.Getter;
import lombok.extern.log4j.Log4j;
import net.rafkos.mc.plugins.BungeePNet.common.Config;
import net.rafkos.mc.plugins.BungeePNet.common.HeartbeatMonitor;
import net.rafkos.mc.plugins.BungeePNet.common.HeartbeatTransmitter;
import net.rafkos.mc.plugins.BungeePNet.common.interfaces.IClient;
import net.rafkos.mc.plugins.BungeePNet.spigot.listeners.SpigotClientListener;
import nl.pvdberg.pnet.client.Client;
import nl.pvdberg.pnet.client.util.AsyncClient;
import nl.pvdberg.pnet.client.util.PlainClient;
import nl.pvdberg.pnet.client.util.TLSClient;
import nl.pvdberg.pnet.event.AsyncListener;
import nl.pvdberg.pnet.packet.Packet;

@Log4j
public class SpigotClient implements IClient
{

	private String host;
	private int port;
	@Getter
	private AsyncClient asyncClient;
	@Getter
	private HeartbeatTransmitter heartBeatTransmitter;
	@Getter
	private HeartbeatMonitor heartBeatMonitor;

	public SpigotClient(String host, int port)
	{
		this.host = host;
		this.port = port;
		init();
	}

	public synchronized boolean connect()
	{
		if(this.asyncClient.isConnected())
		{
			return true;
		}
		
		if(this.asyncClient.connect(this.host, this.port))
		{
			heartBeatTransmitter = new HeartbeatTransmitter(asyncClient, Config.HEARTBEAT_INTERVAL_MILLIS, SpigotApi.getExecutorService());
			heartBeatMonitor = new HeartbeatMonitor(asyncClient, Config.HEARTBEAT_MONITOR_MILLIS, SpigotApi.getExecutorService());
			return true;
		}

		log.error("Unable to connect to the server!");

		return false;
	}

	private synchronized void init()
	{
		Client client = null;
		if(SpigotApi.getClientConfig().isTLSEnabled())
		{
			File trustStore = new File(SpigotApi.getPlugin().getDataFolder().getPath() + "/client/"
					+ SpigotApi.getClientConfig().getTrustStoreFileName());
			try
			{
				byte[] trustStoreBytes = Files.readAllBytes(trustStore.toPath());
				client = new TLSClient(trustStoreBytes,
						SpigotApi.getClientConfig().getTrustStorePassword().toCharArray(),
						SpigotApi.getClientConfig().getTrustStoreType());
			}catch(IOException e)
			{
				log.error("Unable to setup TLS connection. " + e.getLocalizedMessage());
				if(SpigotApi.getClientConfig().isConnectionToServerRequired())
				{
					Bukkit.shutdown();
				}
			}
		}else
		{
			client = new PlainClient();
		}
		this.asyncClient = new AsyncClient(client);
		this.asyncClient.setClientListener(new SpigotClientListener());
		if(!connect())
		{
			if(!SpigotApi.getClientConfig().isReconnectEnabled() && SpigotApi.getClientConfig().isConnectionToServerRequired())
			{
				Bukkit.shutdown();
			}
			// start reconnect task
			else if(SpigotApi.getClientConfig().isReconnectEnabled() && ReconnectTask.isEnabled())
			{
				try
				{
					SpigotApi.getExecutorService().execute(new ReconnectTask(0));
				}
				catch(RejectedExecutionException e)
				{
					log.warn("Unable to execute reconnection task.");
				}
			}
		}
	}

	@Override
	public synchronized boolean isConnected()
	{
		return this.asyncClient.isConnected();
	}

	@Override
	public synchronized void send(Packet p)
	{
		sendAsync(p, new AsyncListener()
		{
			@Override
			public void onCompletion(boolean success)
			{
				if(!success)
				{
					log.error("Message '" + p + "' not sent correctly");
				}
			}
		});
	}

	@Override
	public synchronized void sendAsync(Packet p, AsyncListener l)
	{
		this.asyncClient.sendAsync(p, l);
	}

	@Override
	public synchronized void stop()
	{
		this.asyncClient.close();
	}

}
