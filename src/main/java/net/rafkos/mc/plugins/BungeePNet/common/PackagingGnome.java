/**
 * 
 * Copyright (C) 2018 Rafał Kosyl <admin@rafkos.net>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
*/
package net.rafkos.mc.plugins.BungeePNet.common;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import lombok.SneakyThrows;

public class PackagingGnome
{
	@SneakyThrows
	public static byte[] packObject(Object obj)
	{
		ByteArrayOutputStream baos = null;
		ObjectOutputStream oos = null;
		try
		{
			baos = new ByteArrayOutputStream();
			oos = new ObjectOutputStream(baos);
			
			oos.writeObject(obj);
			return baos.toByteArray();
		}catch(IOException e)
		{
			e.printStackTrace();
		}finally
		{
			if(oos != null)
			{
				oos.close();
			}
			if(baos != null)
			{
				baos.close();
			}
		}
		return null;
	}

	@SneakyThrows
	public static Object unpackObject(byte[] bytes) throws ClassNotFoundException
	{

		ByteArrayInputStream bais = null;
		ObjectInputStream ois = null;
		try
		{
			bais = new ByteArrayInputStream(bytes);
			ois = new ObjectInputStream(bais);
			return ois.readObject();
		}catch(IOException e)
		{
			e.printStackTrace();
		}finally
		{
			if(ois != null)
			{
				ois.close();
			}
			if(bais != null)
			{
				bais.close();
			}
		}

		return null;
	}

}
