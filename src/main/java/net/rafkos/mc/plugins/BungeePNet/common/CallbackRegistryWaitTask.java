/**
 * 
 * Copyright (C) 2018 Rafał Kosyl <admin@rafkos.net>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
*/
package net.rafkos.mc.plugins.BungeePNet.common;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j;

/**
 * Purpose of this task is to wait for all callbacks to either complete or throw
 * timeout exception.
 */
@AllArgsConstructor
@Getter
@Log4j
public class CallbackRegistryWaitTask implements Runnable
{

	private int maxChecks;
	private CallbackRegistry registry;

	@Override
	@SneakyThrows
	public void run()
	{
		log.info("Callbacks wait task started...");

		int checksCounter = 0;

		while(!registry.isEmpty())
		{
			log.info("Waiting for callbacks to complete... [" + (checksCounter + 1) + "/" + maxChecks + "]");
			Thread.sleep(Config.CALLBACK_WAIT_TASK_TIME_MILLIS);
			if(++checksCounter >= maxChecks)
			{
				log.info("Terminating callback wait task...");
			}
		}

		if(registry.isEmpty())
		{
			log.info("Callback registry is empty.");
		}
		else
		{
			log.warn("Callback registry has terminated with callbacks inside.");
		}

	}

}
