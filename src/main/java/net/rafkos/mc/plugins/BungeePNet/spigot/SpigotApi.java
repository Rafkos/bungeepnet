/**
 * 
 * Copyright (C) 2018 Rafał Kosyl <admin@rafkos.net>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
*/
package net.rafkos.mc.plugins.BungeePNet.spigot;

import java.io.File;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j;
import net.rafkos.mc.plugins.BungeePNet.common.CallbackRegistry;
import net.rafkos.mc.plugins.BungeePNet.common.CallbackRegistryWaitTask;
import net.rafkos.mc.plugins.BungeePNet.common.Config;
import net.rafkos.mc.plugins.BungeePNet.common.listeners.ServerConnectionListenerManager;
import net.rafkos.mc.plugins.BungeePNet.spigot.handlers.packets.redirectors.RequestRedirectManager;
import net.rafkos.mc.plugins.BungeePNet.spigot.listeners.PluginListener;
import net.rafkos.utils.ConfigLoader.ConfigLoader;
import net.rafkos.utils.ResourceUnpacker.ResourceUnpacker;

@Log4j
public class SpigotApi
{
	@Getter
	@Setter
	private static ActionTransmitter actionTransmitter;
	@Getter
	@Setter
	private static CallbackRegistry callbackRegistry;
	@Getter
	@Setter
	private static SpigotClient client;
	@Getter
	@Setter
	private static ClientConfig clientConfig;
	@Getter
	@Setter
	private static ScheduledExecutorService executorService;
	private static boolean loaded = false;
	@Getter
	@Setter
	private static JavaPlugin plugin;
	@Getter
	@Setter
	private static RequestRedirectManager requestRedirectManager;
	@Getter
	@Setter
	private static ServerConnectionListenerManager serverConnectionListenerManager;

	@SneakyThrows
	public static void disable()
	{
		if(getClient().getHeartBeatMonitor() != null)
		{
			getClient().getHeartBeatMonitor().terminate();
		}
		if(getClient().getHeartBeatTransmitter() != null)
		{
			getClient().getHeartBeatTransmitter().terminate();
		}
		
		ReconnectTask.setDisabled(true);
		final CallbackRegistryWaitTask callbackWaitTask = new CallbackRegistryWaitTask(Config.NUM_OF_CHECKS_BEFORE_TERMINATION_OF_CALLBACKS_REGISTRY,
				getCallbackRegistry());

		getServerConnectionListenerManager().unregisterAllListeners();
		if(getClient() != null)
		{
			getClient().stop();
		}
		
		getCallbackRegistry().getScheduler().shutdown();

		getExecutorService().execute(callbackWaitTask);
		getExecutorService().shutdown();
		getExecutorService().awaitTermination(120, TimeUnit.SECONDS);

	}

	public static void init(JavaPlugin plugin)
	{
		if(loaded)
		{
			new Exception("Cannot start SpigotApi twice.");
		}
		loaded = true;
		setPlugin(plugin);
		getPlugin().getServer().getPluginManager().registerEvents(new PluginListener(), getPlugin());
		setExecutorService(Executors.newScheduledThreadPool(Config.DEFAULT_SIZE_OF_GENERIC_EXECUTOR_POOL));
		setServerConnectionListenerManager(new ServerConnectionListenerManager());

		ResourceUnpacker.copyConfigFilesFromJarIfMissing(SpigotApi.class,
				new File(getPlugin().getDataFolder().getPath() + "/client/"), "TLS",
				"default-bungeepnet-truststore.p12");

		ClientConfig config = null;
		if((config = (ClientConfig) ConfigLoader.load(ClientConfig.class,
				new File(getPlugin().getDataFolder().getPath() + "/client/"), "spigot_client.json")) == null)
		{
			log.error("BungeePNet requires working client configuration to run, exiting...");
			Bukkit.shutdown();
		}
		log.warn("Remember to change default TLS password/keys and other config!");

		setClientConfig(config);
		setRequestRedirectManager(new RequestRedirectManager());
		setClient(new SpigotClient(getClientConfig().getHostIp(), getClientConfig().getHostPort()));
		setCallbackRegistry(new CallbackRegistry());
		setActionTransmitter(new ActionTransmitter());
	}

}
