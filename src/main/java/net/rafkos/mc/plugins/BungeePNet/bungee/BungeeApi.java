/**
 * 
 * Copyright (C) 2018 Rafał Kosyl <admin@rafkos.net>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
*/
package net.rafkos.mc.plugins.BungeePNet.bungee;

import java.io.File;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Plugin;
import net.rafkos.mc.plugins.BungeePNet.bungee.handlers.redirectors.RequestRedirectManager;
import net.rafkos.mc.plugins.BungeePNet.bungee.handlers.redirectors.ResponseRedirectManager;
import net.rafkos.mc.plugins.BungeePNet.common.CallbackRegistry;
import net.rafkos.mc.plugins.BungeePNet.common.CallbackRegistryWaitTask;
import net.rafkos.mc.plugins.BungeePNet.common.Config;
import net.rafkos.mc.plugins.BungeePNet.common.listeners.ClientConnectionListenerManager;
import net.rafkos.utils.ConfigLoader.ConfigLoader;
import net.rafkos.utils.ResourceUnpacker.ResourceUnpacker;

@Log4j
public class BungeeApi
{
	@Getter
	@Setter
	private static ClientHeartbeatTransmitterRegistry heartbeatTransmitterRegistry;
	@Getter
	@Setter
	private static ClientHeartbeatMonitorRegistry heartbeatMonitorRegistry;
	@Getter
	@Setter
	private static ActionTransmitter actionTransmitter;
	@Getter
	@Setter
	private static CallbackRegistry callbackRegistry;
	@Getter
	@Setter
	private static ClientConnectionListenerManager clientConnectionListenerManager;
	@Getter
	@Setter
	private static ClientNameRegistry clientNameRegistry;
	@Getter
	@Setter
	private static ScheduledExecutorService executorService;
	private static boolean loaded = false;
	@Getter
	@Setter
	private static Plugin plugin;
	@Getter
	@Setter
	private static RequestRedirectManager requestRedirectManager;
	@Getter
	@Setter
	private static ResponseRedirectManager responseRedirectManager;
	@Getter
	@Setter
	private static BungeeServer server;
	@Getter
	@Setter
	private static BungeeServerConfig serverConfig;

	@SneakyThrows
	public static void disable()
	{

		getHeartbeatMonitorRegistry().unregisterAll();
		getHeartbeatTransmitterRegistry().unregisterAll();

		final CallbackRegistryWaitTask callbackWaitTask = new CallbackRegistryWaitTask(
				Config.NUM_OF_CHECKS_BEFORE_TERMINATION_OF_CALLBACKS_REGISTRY, getCallbackRegistry());

		getCallbackRegistry().getScheduler().shutdown();

		getExecutorService().execute(callbackWaitTask);
		getExecutorService().shutdown();
		getExecutorService().awaitTermination(120, TimeUnit.SECONDS);

		getClientConnectionListenerManager().unregisterAllListeners();

		if(getServer() != null)
		{
			getServer().stop();
		}
	}

	@SneakyThrows
	public static void init(Plugin plugin)
	{
		if(loaded)
		{
			new Exception("Cannot start BungeeApi twice.");
		}
		loaded = true;
		setPlugin(plugin);
		setExecutorService(Executors.newScheduledThreadPool(Config.DEFAULT_SIZE_OF_GENERIC_EXECUTOR_POOL));
		setHeartbeatMonitorRegistry(new ClientHeartbeatMonitorRegistry());
		setHeartbeatTransmitterRegistry(new ClientHeartbeatTransmitterRegistry());
		setClientConnectionListenerManager(new ClientConnectionListenerManager());

		ResourceUnpacker.copyConfigFilesFromJarIfMissing(BungeeApi.class,
				new File(getPlugin().getDataFolder().getPath() + "/server/"), "TLS", "default-bungeepnet-keystore.p12");

		BungeeServerConfig serverConfig = null;
		if((serverConfig = (BungeeServerConfig) ConfigLoader.load(BungeeServerConfig.class,
				new File(getPlugin().getDataFolder().getPath() + "/server/"), "bungee_server.json")) == null)
		{
			log.error("BungeePNet requires working server configuration to run, exiting...");
			ProxyServer.getInstance().stop();
		}
		setServerConfig(serverConfig);

		log.warn("Remember to change default TLS password/keys and other config!");

		setActionTransmitter(new ActionTransmitter());
		setCallbackRegistry(new CallbackRegistry());
		setRequestRedirectManager(new RequestRedirectManager());
		setResponseRedirectManager(new ResponseRedirectManager());

		setClientNameRegistry(new ClientNameRegistry());
		setServer(new BungeeServer(getServerConfig().getHostPort()));
		// wait for the server to start
		while(!getServer().isRunning())
		{
			Thread.sleep(50);
		}

	}

}
