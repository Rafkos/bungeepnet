/**
 * 
 * Copyright (C) 2018 Rafał Kosyl <admin@rafkos.net>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
*/
package net.rafkos.mc.plugins.BungeePNet.spigot;

import java.util.HashMap;
import java.util.UUID;

import org.bukkit.entity.Player;

import net.rafkos.mc.plugins.BungeePNet.common.ActionBuilder;
import net.rafkos.mc.plugins.BungeePNet.common.Callback;
import net.rafkos.mc.plugins.BungeePNet.common.Config;
import net.rafkos.mc.plugins.BungeePNet.common.PackagingGnome;
import net.rafkos.mc.plugins.BungeePNet.common.enums.EMessageStatus;
import net.rafkos.mc.plugins.BungeePNet.common.enums.ETargetType;
import net.rafkos.mc.plugins.BungeePNet.common.exceptions.ConnectionFailureException;
import net.rafkos.mc.plugins.BungeePNet.common.interfaces.IAction;
import net.rafkos.mc.plugins.BungeePNet.common.interfaces.ITarget;
import net.rafkos.mc.plugins.BungeePNet.common.interfaces.ITransmitter;
import net.rafkos.mc.plugins.BungeePNet.common.targets.TargetPlayerName;
import net.rafkos.mc.plugins.BungeePNet.common.targets.TargetPlayerUniqueId;
import nl.pvdberg.pnet.packet.Packet.PacketType;
import nl.pvdberg.pnet.packet.PacketBuilder;

public class ActionTransmitter implements ITransmitter
{

	@Override
	public void send(ITarget target, Class<? extends IAction> actionClass, Object[] actionParameters, long timeout,
			Callback callback) throws ConnectionFailureException
	{
		
		// check if target is on this server
		if(target instanceof TargetPlayerName || target instanceof TargetPlayerUniqueId)
		{
			Player player = null;
			if(target instanceof TargetPlayerName)
			{
				player = SpigotApi.getPlugin().getServer().getPlayerExact((String) target.getValue());
			}else if(target instanceof TargetPlayerUniqueId)
			{
				player = SpigotApi.getPlugin().getServer().getPlayer(UUID.fromString(target.getValue()));
			}
			
			// if target player found on this server then execute action
			if(player != null)
			{
				
				IAction action = ActionBuilder.getAction(actionClass.getName(), actionParameters);
				HashMap<String, Object> result = null;
				EMessageStatus status = null;
				Throwable exception = null;
				
				try
				{
					result = action.execute();
					status = EMessageStatus.SUCCESS;
				}catch(Exception e)
				{
					status = EMessageStatus.FAILED;
					exception = e;
				}
				
				switch(status)
				{
				case FAILED:
					callback.getErrorHandler().execute(exception);
					break;
				case SUCCESS:
					callback.getResultHandler().execute(result);
					break;
				default:
					break;
				}
				
				return;
			}
		}
		
		if(SpigotApi.getClient().isConnected())
		{
			UUID callbackUniqueId = UUID.randomUUID();

			if(SpigotApi.getCallbackRegistry().register(callbackUniqueId, callback, timeout))
			{

				PacketBuilder packetBuilder = new PacketBuilder(PacketType.Request);
				packetBuilder.withID(Config.REQUEST_PACKET_ID);
				packetBuilder.withInt(target.getType().getId());
				packetBuilder.withString(target.getValue());
				packetBuilder.withInt(ETargetType.CLIENT_NAME.getId());
				packetBuilder.withString(SpigotApi.getClientConfig().getClientName());
				packetBuilder.withString(callbackUniqueId.toString());
				packetBuilder.withString(actionClass.getName());
				packetBuilder.withBytes(PackagingGnome.packObject(actionParameters));
				
				SpigotApi.getClient().send(packetBuilder.build());
			}else
			{
				throw new IllegalArgumentException("Unable to register callback: " + callback + ".");
			}
		}else
		{
			throw new ConnectionFailureException();
		}
	}

	@Override
	public void send(ITarget target, Class<? extends IAction> actionClass, Object[] actionParameters, long timeout)
			throws ConnectionFailureException
	{
		send(target, actionClass, actionParameters, timeout, new Callback());
	}

	@Override
	public void send(ITarget target, Class<? extends IAction> actionClass, Object[] actionParameters)
			throws ConnectionFailureException
	{
		send(target, actionClass, actionParameters, Config.DEFAULT_TIMEOUT_MILLIS, new Callback());
	}

	@Override
	public void send(ITarget target, Class<? extends IAction> actionClass, Object[] actionParameters, Callback callback)
			throws ConnectionFailureException
	{
		send(target, actionClass, actionParameters, Config.DEFAULT_TIMEOUT_MILLIS, callback);
	}

}
