/**
 * 
 * Copyright (C) 2018 Rafał Kosyl <admin@rafkos.net>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
*/
package net.rafkos.mc.plugins.BungeePNet.common.targets;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import net.rafkos.mc.plugins.BungeePNet.common.enums.ETargetType;
import net.rafkos.mc.plugins.BungeePNet.common.interfaces.ITarget;

@AllArgsConstructor
public class TargetPlayerName implements ITarget, Serializable
{
	private static final long serialVersionUID = -5606676546487710138L;
	private String playerName;

	@Override
	public ETargetType getType()
	{
		return ETargetType.PLAYER_NAME;
	}

	@Override
	public String getValue()
	{
		return playerName;
	}

}
