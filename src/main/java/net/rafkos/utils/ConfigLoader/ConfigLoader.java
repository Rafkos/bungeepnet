/**
 * 
 * Copyright (C) 2018 Rafał Kosyl <admin@rafkos.net>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
*/
package net.rafkos.utils.ConfigLoader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j;

@Log4j
public class ConfigLoader
{
	private static Gson gson = new Gson();

	@SneakyThrows
	public static Object load(Class<?> targetClass, File directory, String filename)
	{
		Object o = null;
		File file = new File(directory + "/" + filename);
		FileReader reader = null;
		try
		{
			reader = new FileReader(file);
			o = gson.fromJson(reader, targetClass);
			log.info("Configuration file '" + file.getPath() + "' loaded.");
		}catch(FileNotFoundException e1)
		{
			log.warn("Unable to locate configuration file '" + file.getPath() + "', creating one from memory.");
			try
			{
				if(save(targetClass.getConstructor().newInstance(), directory, filename))
				{
					return load(targetClass, directory, filename);
				}
			}catch(InstantiationException e)
			{
				log.error("Internal loader error. Unable to initialize configuration class " + targetClass + " "
						+ e.getLocalizedMessage());
			}catch(IllegalAccessException e)
			{
				log.error("Unable to load Json config '" + file.getPath() + "' due to an illegal access exception. "
						+ e.getLocalizedMessage());
			}
		}catch(JsonSyntaxException | JsonIOException e)
		{
			log.error("Json syntax or IO error has occured when loading '" + file.getPath()
					+ "'. Please check your configuration file. " + e.getLocalizedMessage());
		}finally
		{
			if(reader != null)
			{
				reader.close();
			}
		}
		return o;
	}

	public static boolean save(Object o, File directory, String filename)
	{
		File file = new File(directory + "/" + filename);
		if(!directory.isDirectory())
		{
			if(directory.mkdirs())
			{
				log.info("Configuration directory created in '" + directory.getPath() + "'.");
			}else
			{
				log.error("Unable to create configuration directory: '" + directory.getPath() + "'.");
				return false;
			}
		}

		try
		{
			PrintWriter out = new PrintWriter(file);
			GsonBuilder builder = new GsonBuilder();
			builder.setPrettyPrinting();
			builder.create().toJson(o, out);
			out.close();
			return true;
		}catch(JsonIOException e)
		{
			log.error("Json syntax or IO error. Please check your configuration file. " + e.getLocalizedMessage());
		}catch(FileNotFoundException e)
		{
			log.error("Unable to locate file to write. " + e.getLocalizedMessage());
		}

		return false;
	}

}
