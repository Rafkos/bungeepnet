/**
 * 
 * Copyright (C) 2018 Rafał Kosyl <admin@rafkos.net>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
*/
package net.rafkos.utils.ResourceUnpacker;

import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import lombok.SneakyThrows;

public class ResourceUnpacker
{

	@SneakyThrows
	public static void copyConfigFilesFromJarIfMissing(Class<?> owner, File destinationDirectory, String zipFolder,
			String filename)
	{

		// ensure data folder exists
		if(!destinationDirectory.exists())
		{
			destinationDirectory.mkdirs();
		}

		// URL to jar file
		URL jar = owner.getProtectionDomain().getCodeSource().getLocation();

		ZipFile zf = new ZipFile(URLDecoder.decode(jar.getPath(), "UTF-8"));

		@SuppressWarnings("unchecked")
		Enumeration<ZipEntry> entries = (Enumeration<ZipEntry>) zf.entries();

		while(entries.hasMoreElements())
		{
			ZipEntry e = entries.nextElement();

			if(e == null)
			{
				break;
			}
			String entry = e.getName();

			if(entry.equalsIgnoreCase(zipFolder + "/" + filename))
			{

				// a path to the data folder + name of the file
				File local = new File(destinationDirectory.getPath() + "/" + filename);

				// check if the file exists
				if(!local.exists())
				{
					// if not then copy it from the jar
					InputStream is = zf.getInputStream(e);
					Files.copy(is, Paths.get(local.getAbsolutePath()));
					is.close();
				}
			}
		}

		zf.close();

	}

}
