/**
 * 
 * Copyright (C) 2018 Rafał Kosyl <admin@rafkos.net>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
*/
package net.rafkos.mc.plugins.BungeePNet.examples.spigot;

import org.bukkit.plugin.java.JavaPlugin;

import net.rafkos.mc.plugins.BungeePNet.common.Callback;
import net.rafkos.mc.plugins.BungeePNet.common.exceptions.ConnectionFailureException;
import net.rafkos.mc.plugins.BungeePNet.common.targets.TargetBungeeServer;
import net.rafkos.mc.plugins.BungeePNet.examples.common.GetOnlinePlayersCountAction;
import net.rafkos.mc.plugins.BungeePNet.spigot.SpigotApi;

/**
 * Main plugin class. This plugin's jar should be placed in every Spigot, Bungee instance.
 */
public class ExamplePlugin extends JavaPlugin
{

	@Override
	public void onEnable()
	{
		/*
		 * Let's say we want to obtain a number of online players every 40 ticks. Create a
		 * regular Spigot task.
		 */
		this.getServer().getScheduler().scheduleSyncRepeatingTask(this, () ->
		{
			try
			{
				obtainOnlinePlayersCount();
			} catch (ConnectionFailureException e)
			{
				e.printStackTrace();
			}
		}, 20, 40);
	}
	
	private void obtainOnlinePlayersCount() throws ConnectionFailureException
	{
		/*
		 * To send a message to another Spigot or Bungee use SpigotApi's ActionTransmitter.
		 */
		SpigotApi.getActionTransmitter()
				// specify target
				.send(new TargetBungeeServer(), 
						// specify action to perform on the target
						GetOnlinePlayersCountAction.class, null, 2000,
				// create callback - a piece of code to execute when received an answer from target
				new Callback((receivedResult) ->
				{
					// obtain players count from results and do whatever you like with it
					int count = (int) receivedResult.get("players_count");
					System.out.println("online players: " + count);
				
				// same as above but this piece of code will execute only if any errors occurred
				}, (error) ->
				{
					error.printStackTrace();
				}));
	}

}
