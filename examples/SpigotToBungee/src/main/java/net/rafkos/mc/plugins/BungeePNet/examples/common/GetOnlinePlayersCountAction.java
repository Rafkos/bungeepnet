/**
 * 
 * Copyright (C) 2018 Rafał Kosyl <admin@rafkos.net>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
*/
package net.rafkos.mc.plugins.BungeePNet.examples.common;

import java.util.HashMap;
import net.md_5.bungee.api.plugin.Plugin;
import net.rafkos.mc.plugins.BungeePNet.bungee.BungeeApi;
import net.rafkos.mc.plugins.BungeePNet.common.interfaces.IAction;

public class GetOnlinePlayersCountAction implements IAction
{

	/**
	 * This action will be performed on Bungee proxy. 
	 * Any code inside this method should be compatible with Bungee's classes.
	 */
	@Override
	public HashMap<String, Object> execute()
	{
		/*
		 * You can store anything you want inside of this map, but remember
		 * that Spigot needs to have all the definitions of provided classes.
		 */
		HashMap<String, Object> result = new HashMap<>();
		
		/*
		 * Calling BungeeApi in order to retrieve Bungee plugin.
		 */
		Plugin bungeePlugin = BungeeApi.getPlugin();
		
		/*
		 * Store online players count inside of the map under any key you like.
		 */
		int playersCount = bungeePlugin.getProxy().getOnlineCount();
		result.put("players_count", playersCount);
		
		/*
		 * Return results map.
		 */
		return result;
	}

}
