/**
 * 
 * Copyright (C) 2018 Rafał Kosyl <admin@rafkos.net>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
*/
package net.rafkos.mc.plugins.BungeePNet.examples.bungee;

import net.md_5.bungee.api.plugin.Plugin;
import net.rafkos.mc.plugins.BungeePNet.common.listeners.AClientConnectionListener;

public class ExampleListener extends AClientConnectionListener
{
	private Plugin plugin;

	public ExampleListener(Plugin plugin)
	{
		this.plugin = plugin;
	}
	
	/**
	 * This method will be called when client connects.
	 */
	@Override
	public void onClientConnected(String clientName)
	{
		plugin.getLogger().info("A new client has connected: "+clientName);
		// execute your plugin's logic
	}

	/**
	 * This method will be called when one of the clients disconnects.
	 */
	@Override
	public void onClientDisconnected(String clientName)
	{
		plugin.getLogger().info("A client has disconnected: "+clientName);
		// execute your plugin's logic
	}

}
