/**
 * 
 * Copyright (C) 2018 Rafał Kosyl <admin@rafkos.net>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
*/
package net.rafkos.mc.plugins.BungeePNet.examples.bungee;


import net.md_5.bungee.api.plugin.Plugin;
import net.rafkos.mc.plugins.BungeePNet.bungee.BungeeApi;

/**
 * Main plugin class.
 */
public class ExamplePlugin6 extends Plugin
{
	private ExampleListener listener;

	@Override
	public void onEnable()
	{
		/*
		 * Let's say we want to respond to Spigot client connection or disconnection.
		 * We may want to send some configuration to client or stop our plugin's logic once a connection
		 * with client is lost.
		 * First let's create and register a custom listener
		 */
		listener = new ExampleListener(this);
		BungeeApi.getClientConnectionListenerManager().registerListener(this, listener);
		
	}
	
	@Override
	public void onDisable()
	{
		/*
		 * Remember to unregister the listener.
		 */
		BungeeApi.getClientConnectionListenerManager().unregisterListener(this, listener);
	}
	
}
