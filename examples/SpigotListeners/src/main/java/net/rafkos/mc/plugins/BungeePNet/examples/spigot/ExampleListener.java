/**
 * 
 * Copyright (C) 2018 Rafał Kosyl <admin@rafkos.net>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
*/
package net.rafkos.mc.plugins.BungeePNet.examples.spigot;

import org.bukkit.plugin.java.JavaPlugin;

import net.rafkos.mc.plugins.BungeePNet.common.listeners.AServerConnectionListener;

public class ExampleListener extends AServerConnectionListener
{
	private JavaPlugin plugin;

	public ExampleListener(JavaPlugin plugin)
	{
		this.plugin = plugin;
	}
	
	/**
	 * This method will be called every time Spigot disconnects from the server.
	 */
	@Override
	public void onServerDisconnected()
	{
		plugin.getLogger().info("You have lost connection to Bungee server!");
		// execute your plugin's logic
	}

	/**
	 * This method will be called only when connecting first time.
	 * In case this plugin wasn't enabled during connecting this method
	 * will be executed as soon as this plugin loads.
	 */
	@Override
	public void onServerConnected()
	{
		plugin.getLogger().info("You have been connected to Bungee server!");
		// execute your plugin's logic
	}

	/**
	 * This method will be called every time Spigot reconnects after
	 * disconnection from the server. It will not be called when connecting
	 * for the first time.
	 */
	@Override
	public void onServerReconnected()
	{
		plugin.getLogger().info("You have been reconnected to Bungee server!");
		// execute your plugin's logic
	}

}
