/**
 * 
 * Copyright (C) 2018 Rafał Kosyl <admin@rafkos.net>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
*/
package net.rafkos.mc.plugins.BungeePNet.examples.spigot;

import org.bukkit.plugin.java.JavaPlugin;

import net.rafkos.mc.plugins.BungeePNet.spigot.SpigotApi;

/**
 * Main plugin class.
 */
public class ExamplePlugin5 extends JavaPlugin
{
	private ExampleListener listener;

	@Override
	public void onEnable()
	{
		/*
		 * Let's say we want to respond to Bungee's server disconnection.
		 * We may want to save data or stop our plugin's logic once a connection
		 * with Bungee server is lost.
		 * First let's create and register a custom listener
		 */
		listener = new ExampleListener(this);
		SpigotApi.getServerConnectionListenerManager().registerListener(this, listener);
		
	}
	
	@Override
	public void onDisable()
	{
		/*
		 * Remember to unregister the listener.
		 */
		SpigotApi.getServerConnectionListenerManager().unregisterListener(this, listener);
	}
	
}
