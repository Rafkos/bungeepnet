/**
 * 
 * Copyright (C) 2018 Rafał Kosyl <admin@rafkos.net>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
*/
package net.rafkos.mc.plugins.BungeePNet.examples.bungee;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import net.md_5.bungee.api.plugin.Plugin;
import net.rafkos.mc.plugins.BungeePNet.bungee.BungeeApi;
import net.rafkos.mc.plugins.BungeePNet.common.Callback;
import net.rafkos.mc.plugins.BungeePNet.common.exceptions.ConnectionFailureException;
import net.rafkos.mc.plugins.BungeePNet.common.exceptions.TargetNotFoundException;
import net.rafkos.mc.plugins.BungeePNet.common.targets.TargetClientName;
import net.rafkos.mc.plugins.BungeePNet.examples.common.SayHelloToEveryone;

/**
 * Main plugin class. This plugin's jar should be placed in every Spigot, Bungee instance.
 */
public class ExamplePlugin3 extends Plugin
{

	@Override
	public void onEnable()
	{
		/*
		 * Let's say we want to say hello to everyone on given client every 2 seconds.
		 *  Create a regular Bungee task.
		 */
		this.getProxy().getScheduler().schedule(this, () ->
		{
			try
			{
				sayHelloToEveryoneOnSpigot("lobby");
			} catch (ConnectionFailureException e)
			{
				e.printStackTrace();
			}
		}, 500, 2000, TimeUnit.MILLISECONDS);
	}
	
	private void sayHelloToEveryoneOnSpigot(String clientName) throws ConnectionFailureException
	{
		/*
		 * To send a message to another Bungee or Spigot use BungeeApi's ActionTransmitter.
		 */
		BungeeApi.getActionTransmitter()
				// specify target
				.send(new TargetClientName(clientName), 
						// specify action to perform on the target
						SayHelloToEveryone.class, null, 2000,
				// create callback - a piece of code to execute when received an answer from target
				new Callback((receivedResult) ->
				{
					// obtain greeted players' list from results and do whatever you like with it
					@SuppressWarnings("unchecked")
					ArrayList<String> playerNames = (ArrayList<String>) receivedResult.get("greeted_players");
					if(playerNames.isEmpty())
					{
						System.out.println("No players greeted.");
					}
					else
					{
						System.out.println("Greeted players: "+playerNames.toString());
					}
				
				// same as above but this piece of code will execute only if any errors occurred
				}, (error) ->
				{
					// check if Spigot client was found
					if(error instanceof TargetNotFoundException)
					{
						System.out.println("Unable to locate Spigot '"+clientName+"'");
					}
					// some other error
					else
					{
						error.printStackTrace();
					}
				}));
	}

}
