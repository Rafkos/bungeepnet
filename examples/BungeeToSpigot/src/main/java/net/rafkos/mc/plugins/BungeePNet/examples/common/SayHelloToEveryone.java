/**
 * 
 * Copyright (C) 2018 Rafał Kosyl <admin@rafkos.net>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
*/
package net.rafkos.mc.plugins.BungeePNet.examples.common;

import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.entity.Player;

import net.rafkos.mc.plugins.BungeePNet.common.interfaces.IAction;
import net.rafkos.mc.plugins.BungeePNet.spigot.SpigotApi;

public class SayHelloToEveryone implements IAction
{

	/**
	 * This action will be performed on Spigot client. 
	 * Any code inside this method should be compatible with Spigot's classes.
	 */
	@Override
	public HashMap<String, Object> execute()
	{
		/*
		 * You can store anything you want inside of this map, but remember
		 * that Bungee needs to have all the definitions of provided classes.
		 */
		HashMap<String, Object> results = new HashMap<>();
		
		/*
		 * Store greeted players' names in list.
		 */
		ArrayList<String> playerNames = new ArrayList<>();
		
		for(Player p: SpigotApi.getPlugin().getServer().getOnlinePlayers())
		{
			playerNames.add(p.getName());
			/*
			 * Say hello
			 */
			p.sendMessage("Hello, "+p.getName()+"!");
		}
		
		/*
		 * Store greeted players inside map 
		 */
		results.put("greeted_players", playerNames);
		
		/*
		 * Return results map.
		 */
		return results;
	}

}
