/**
 * 
 * Copyright (C) 2018 Rafał Kosyl <admin@rafkos.net>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
*/
package net.rafkos.mc.plugins.BungeePNet.examples.common;

import java.util.HashMap;

import org.bukkit.entity.Player;

import net.rafkos.mc.plugins.BungeePNet.common.interfaces.IAction;
import net.rafkos.mc.plugins.BungeePNet.spigot.SpigotApi;

public class MessagePlayerWithHello implements IAction
{
	private String playerName;
	
	public MessagePlayerWithHello(String playerName)
	{
		this.playerName = playerName;
	}

	/**
	 * This action will be performed on target's Spigot server. 
	 * Any code inside this method should be compatible with Spigot's classes.
	 */
	@Override
	public HashMap<String, Object> execute()
	{
		/*
		 * Always create a map to store results.
		 */
		HashMap<String, Object> results = new HashMap<>();
		
		/*
		 * Put some example data inside.
		 */
		results.put("delivery_time", System.currentTimeMillis());
		
		/*
		 * Get Player from server
		 */
		Player p = SpigotApi.getPlugin().getServer().getPlayer(playerName);
		
		/*
		 * Player not found. In this case the player was found during searching servers in Bungee
		 * and that's why this server received this action. There are chances that player disconnected
		 * right before this action's execution.
		 */
		if(p == null)
		{
			results.put("delivered", false);
		}
		else
		{
			p.sendMessage("Hello");
			results.put("delivered", true);
		}
		
		return results;
	}

}
