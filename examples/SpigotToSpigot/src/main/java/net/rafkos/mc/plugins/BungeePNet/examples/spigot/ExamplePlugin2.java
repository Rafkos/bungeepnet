/**
 * 
 * Copyright (C) 2018 Rafał Kosyl <admin@rafkos.net>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
*/
package net.rafkos.mc.plugins.BungeePNet.examples.spigot;

import org.bukkit.plugin.java.JavaPlugin;

import net.rafkos.mc.plugins.BungeePNet.common.Callback;
import net.rafkos.mc.plugins.BungeePNet.common.exceptions.ConnectionFailureException;
import net.rafkos.mc.plugins.BungeePNet.common.exceptions.TargetNotFoundException;
import net.rafkos.mc.plugins.BungeePNet.common.targets.TargetPlayerName;
import net.rafkos.mc.plugins.BungeePNet.examples.common.MessagePlayerWithHello;
import net.rafkos.mc.plugins.BungeePNet.spigot.SpigotApi;

/**
 * Main plugin class. This plugin's jar should be placed in every Spigot instance.
 */
public class ExamplePlugin2 extends JavaPlugin
{

	@Override
	public void onEnable()
	{
		/*
		 * Let's say we want to send a message to some player every 40 ticks and all we
		 * know is his name. First let's use a regular Spigot scheduler to schedule the
		 * task.
		 */
		this.getServer().getScheduler().scheduleSyncRepeatingTask(this, () ->
		{
			try
			{
				sayHelloToPlayer("The_Bad_Ogre");
			}catch(ConnectionFailureException e)
			{
				e.printStackTrace();
			}
		}, 20, 40);
	}

	private void sayHelloToPlayer(String playerName) throws ConnectionFailureException
	{
		/*
		 * To send a message to another Spigot or Bungee use SpigotApi's
		 * ActionTransmitter.
		 */
		SpigotApi.getActionTransmitter()
				// specify target
				.send(new TargetPlayerName(playerName),
						// specify action to perform on the target
						MessagePlayerWithHello.class, new Object[] {playerName}, 2000,
				// create callback - a piece of code to execute when received an answer from
				// target
				new Callback((receivedResult) ->
				{
					// obtain delivery time from results
					long deliveryTime = (long) receivedResult.get("delivery_time");
					boolean delivered = (boolean) receivedResult.get("delivered");
					if(delivered)
					{
						System.out.println("Message delivered: " + (System.currentTimeMillis() - deliveryTime)
								+ " milliseconds ago.");
					}else
					{
						System.out.println("Server located but player disconnected before receiving this request");
					}

					// same as above but this piece of code will execute only if any errors occurred
				}, (error) ->
				{
					// check if player was found
					if(error instanceof TargetNotFoundException)
					{
						System.out.println("Unable to locate server of player " + playerName);
					}
					// some other error
					else
					{
						error.printStackTrace();
					}
				}));
	}

}
